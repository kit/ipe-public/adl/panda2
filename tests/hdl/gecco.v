`timescale 1ns / 1ps

module gecco
(
    input wire GECCO_SCLK,
    input wire GECCO_SDI, 
    input wire GECCO_SLD
);

reg [39:0] sr =40'b0;

reg [13:0] dac1 = 14'b0;
reg [13:0] dac2 = 14'b0;
reg [7:0] lds = 8'b0;

always @(posedge GECCO_SCLK)
    sr <= {sr[38:0], GECCO_SDI};

always @(posedge GECCO_SLD)
    {lds[0],lds[1],lds[2],lds[3],lds[4],lds[5],lds[6],lds[7]} <= sr[7:0];

always @(posedge lds[2]) begin
    dac1 <= sr[23:10];
    dac2 <= sr[39:26];
end

endmodule