/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype none


module OSERDESE2 #(
    parameter DATA_RATE_OQ = "DDR",
    parameter DATA_WIDTH = 4,
    parameter SERDES_MODE = "MASTER"
) (
    output wire OQ,
    output wire OFB,
    output wire TQ,
    output wire TFB,
    output wire SHIFTOUT1,
    output wire SHIFTOUT2,
    input wire CLK,
    input wire CLKDIV,
    input wire D1,
    input wire D2,
    input wire D3,
    input wire D4,
    input wire D5,
    input wire D6,
    input wire D7,
    input wire D8,
    input wire TCE,
    input wire OCE,
    input wire TBYTEIN,
    output wire TBYTEOUT,
    input wire RST,
    input wire SHIFTIN1,
    input wire SHIFTIN2,
    input wire T1,
    input wire T2,
    input wire T3,
    input wire T4
);

assign OQ = CLKDIV? (CLK? D1:D2):(CLK? D3:D4);

endmodule
