import time
import numpy as np
from numba import njit
import tables
import logging

logger = logging.getLogger()


hit_dtype = [("event_number", "<i8"), ("col", "<u1"), ("row", "<u2"), ('comp', 'u1'),
             ("ts11", "<u2"), ("ts12", "<u2"), ("ts2", "<u2"), ("ts3", "<u1"),
             ("timestamp", "<u2"), ('err', '<u1')]
ts_dtype = [("event_number", "<i8"), ("timestamp", "<i8"), ('err', '<u1')]
# data fromat from FPGA
#  0    1    2    3   4   5   6    7
# C0, cnt, cnt, cnt,
# C1, col, row, ts3, ts, ts, ts, ts2
# More precisely,
# {K_28_0, 8'hAA, K_28_0, 8'hAA}
# {8'hC0, timestamp_bin_counter[15:8], timestamp_bin_counter[7:0], timestamp_gray_value[7:0]};
# {8'hC1,2'd0, ColAddFromDet[4:0], RowAddFromDet[9:0], TS3FromDet[6:0]};
# {2'd0, TS12FromDet[10:0],TS11FromDet[10:0], TS2FromDet[9:0]};


@njit
def decode_gray(gray, nbits):
    binary = gray & (1 << (nbits - 1))
    for i in range(nbits - 2, -1, -1):
        binary = binary | ((gray ^ (binary >> 1)) & (1 << i))
    return binary


@njit
def _interpret(raw, buf, buf_ts, buf_trig, idx_offset=0):
    idx = 0
    r_i = 0
    t_i = -1
    while idx < len(raw):
        # print(idx, '0x{0:x}: t_i {1}'.format(raw[idx], t_i), ':', hex(buf[t_i]['timestamp']), buf[t_i])
        # ################
        # inj timestamp
        if (raw[idx] & 0xF000_0000) == 0x8000_0000:
            # print('--', idx, '0x{0:x}: t_i {1}'.format(raw[idx], t_i), ':', buf_ts[t_i])
            if raw[idx] & 0x0F00_0000 == 0x300_0000:
                t_i = t_i + 1
                if t_i >= len(buf_ts):
                    return r_i, t_i, idx + 1
                buf_ts[t_i]['timestamp'] = np.int64(raw[idx] & 0x0000_7FFF) << 48
                buf_ts[t_i]['err'] = 0x3
                buf_ts[t_i]['event_number'] = idx + idx_offset
            elif buf_ts[t_i]['err'] == 0x3:
                if raw[idx] & 0x0F00_0000 == 0x200_0000:
                    # print('2', hex(buf_ts[t_i]['timestamp']))
                    buf_ts[t_i]['timestamp'] = buf_ts[t_i]['timestamp'] | (np.int64(raw[idx] & 0x00FF_FFFF) << 24)
                    buf_ts[t_i]['err'] = 0x2
                    # print('2', hex(buf_ts[t_i]['timestamp']))
                else:
                    buf_ts[t_i]['timestamp'] = np.int64(raw[idx])
            elif buf_ts[t_i]['err'] == 0x2:
                if raw[idx] & 0x0F00_0000 == 0x100_0000:
                    # print('1', hex(buf_ts[t_i]['timestamp']))
                    buf_ts[t_i]['timestamp'] = buf_ts[t_i]['timestamp'] | np.int64(raw[idx] & 0x00FF_FFFF)
                    buf_ts[t_i]['err'] = 0x0
                    # print('1', hex(buf_ts[t_i]['timestamp']))
                else:
                    buf_ts[t_i]['timestamp'] = np.int64(raw[idx])
        # ################
        # DUT data
        elif (raw[idx] & 0xF000_0000) == 0x1000_0000:
            # print('++', idx, '0x{0:x}: r_i {1}'.format(raw[idx], r_i), ':', buf[r_i])
            # check word cnter
            info = ((buf[r_i]['err'] >> 4) + 1) & 0xF
            if info == 0xE:
                info == 0
            buf[r_i]['err'] = (buf[r_i]['err'] & 0xF) | (raw[idx] >> 20) & 0xF0
            if (buf[r_i]['err'] >> 4) > 0xD:
                pass
                #print(hex((raw[idx] >> 20) & 0xF0), hex(raw[idx-1]), hex(raw[idx]), hex(raw[idx+1]))
                #raise NotImplementedError()
            elif buf[r_i]['err'] & 0x0F == 0 and (buf[r_i]['err'] >> 4) == 1:
                pass  # this is not error, maybe??
            elif (buf[r_i]['err'] >> 4) != info:
                # print('Unexpceted info r=0x{0:x}, expected_info=0x{1:x}'.format(raw[idx], info))
                buf[r_i]['event_number'] = idx + idx_offset
                buf[r_i]['ts11'] = np.uint16(raw[idx] & 0xFFFF)
                buf[r_i]['ts12'] = np.uint16((raw[idx] >> 16) & 0xFFFF)
                buf[r_i]['row'] = 0xFFFF
                cnt = buf[r_i]['timestamp']
                r_i = r_i + 1
                if r_i >= len(buf):
                    return int(info), t_i, idx + 1
                buf[r_i]['timestamp'] = cnt
                buf[r_i]['err'] = 0x0F
                idx = idx + 1
                continue
            # interprete data
            for d in [(raw[idx] >> 16) & 0xFF, (raw[idx] >> 8) & 0xFF, (raw[idx]) & 0xFF]:
                # print('err={0:x} d=0x{1:x}'.format(buf[r_i]['err'], d))
                # binary counter
                if (buf[r_i]['err'] & 0x0F) in [0, 1, 0xF]:
                    if d == 0x5c and buf[r_i]['err'] & 0x0F == 0:    # fill-data from FPGA
                        buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 0
                    elif d == 0xc1 and buf[r_i]['err'] & 0x0F == 0:  # header of hit
                        buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 6
                    elif d == 0xC0:
                        buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 2
                    elif d == 0x1c:                   #this should not happen
                        buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 0xF
                        buf[r_i]['row'] = 0xFFFE
                    elif d == 0xaa:                   # header
                        buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 1
                    else:
                        # print('Unexpected data d=0x{0:x}(err=0x{1:x})'.format(d, buf[r_i]['err']))
                        buf[r_i]['event_number'] = idx + idx_offset
                        buf[r_i]['ts11'] = np.uint16(raw[idx] & 0xFFFF)
                        buf[r_i]['ts12'] = np.uint16((raw[idx] >> 16) & 0xFFFF)
                        buf[r_i]['row'] = 0xFFFD
                        buf[r_i]['ts2'] = np.uint16(d)
                        cnt = buf[r_i]['timestamp']
                        r_i = r_i + 1
                        if r_i >= len(buf):
                            return int(info), t_i, idx + 1
                        buf[r_i]['timestamp'] = cnt
                        buf[r_i]['err'] = 0xF
                        break
                elif (buf[r_i]['err'] & 0x0F) == 2:
                    buf[r_i]['timestamp'] = np.int16(d << 8)
                    buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 3
                elif (buf[r_i]['err'] & 0x0F) == 3:
                    buf[r_i]['timestamp'] = buf[r_i]['timestamp'] | np.int16(d)
                    buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 4
                elif (buf[r_i]['err'] & 0x0F) == 4:
                    buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 5  # check gray code
                # actual hit data
                elif (buf[r_i]['err'] & 0x0F) == 5:
                    if d == 0xC1:
                        buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 6
                    else:
                        buf[r_i]['event_number'] = idx + idx_offset
                        buf[r_i]['ts11'] = np.uint16(raw[idx] & 0xFFFF)
                        buf[r_i]['ts12'] = np.uint16((raw[idx] >> 16) & 0xFFFF)
                        buf[r_i]['ts2'] = np.uint16(d)
                        buf[r_i]['row'] = 0xFFFC
                        cnt = buf[r_i]['timestamp']
                        info = buf[r_i]['err'] & 0xF0
                        r_i = r_i + 1
                        if r_i >= len(buf):
                            return int(info), t_i, idx + 1
                        buf[r_i]['timestamp'] = cnt
                        buf[r_i]['err'] = info | 0xF
                elif (buf[r_i]['err'] & 0x0F) == 6:
                    if (d & 0xC0) == 0x0:
                        buf[r_i]['col'] = np.uint8(d >> 1) - 2
                        buf[r_i]['row'] = np.uint16(((~d) & 0x1) << 8)
                        buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 7
                    else:
                        buf[r_i]['event_number'] = idx + idx_offset
                        buf[r_i]['ts11'] = np.uint16(raw[idx] & 0xFFFF)
                        buf[r_i]['ts12'] = np.uint16((raw[idx] >> 16) & 0xFFFF)
                        buf[r_i]['ts2'] = np.uint16(d)
                        buf[r_i]['row'] = 0xFFFB
                        cnt = buf[r_i]['timestamp']
                        info = buf[r_i]['err'] & 0xF0
                        r_i = r_i + 1
                        if r_i >= len(buf):
                            return int(info), t_i, idx + 1
                        buf[r_i]['timestamp'] = cnt
                        buf[r_i]['err'] = info | 0xF
                elif (buf[r_i]['err'] & 0x0F) == 7:
                    buf[r_i]['row'] = buf[r_i]['row'] | np.uint16((~d) & 0xFF)  # 0xFF for row=10bits
                    buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 8
                elif (buf[r_i]['err'] & 0x0F) == 8:
                    buf[r_i]['comp'] = np.uint8(((~d) & 0x80) >> 7)  # 0=fast, 1=slow
                    buf[r_i]['ts3'] = np.uint8(decode_gray(d & 0x7F, 7))
                    buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 9
                elif (buf[r_i]['err'] & 0x0F) == 9:
                    if (d & 0xC0) == 0x0:
                        buf[r_i]['ts12'] = np.uint16((d & 0x3F) << 4)
                        buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 10
                    else:
                        buf[r_i]['event_number'] = idx + idx_offset
                        buf[r_i]['ts11'] = np.uint16(raw[idx] & 0xFFFF)
                        buf[r_i]['ts12'] = np.uint16((raw[idx] >> 16) & 0xFFFF)
                        buf[r_i]['ts2'] = np.uint16(d)
                        buf[r_i]['row'] = 0xFFFA
                        cnt = buf[r_i]['timestamp']
                        info = buf[r_i]['err'] & 0xF0
                        r_i = r_i + 1
                        if r_i >= len(buf):
                            return np.int64(info), t_i, idx + 1
                        buf[r_i]['timestamp'] = cnt
                        buf[r_i]['err'] = info | 0xF
                elif (buf[r_i]['err'] & 0x0F) == 10:
                    buf[r_i]['ts12'] = decode_gray(buf[r_i]['ts12'] | np.uint16((d & 0xF0) >> 4), 10)
                    buf[r_i]['ts11'] = np.uint16((d & 0x0F) << 6)
                    buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 11
                elif (buf[r_i]['err'] & 0x0F) == 11:
                    buf[r_i]['ts11'] = decode_gray(buf[r_i]['ts11'] | np.uint16((d & 0xFC) >> 2), 10)
                    buf[r_i]['ts2'] = np.uint16((d & 0x3) << 8)
                    buf[r_i]['err'] = (buf[r_i]['err'] & 0xF0) | 12
                elif (buf[r_i]['err'] & 0x0F) == 12:
                    buf[r_i]['ts2'] = decode_gray(buf[r_i]['ts2'] | np.uint16(d & 0xFF), 10)
                    info = buf[r_i]['err'] & 0xF0
                    buf[r_i]['err'] = 0x0
                    buf[r_i]['event_number'] = idx + idx_offset
                    cnt = buf[r_i]['timestamp']
                    r_i = r_i + 1
                    if r_i >= len(buf):
                        return np.int64(info), t_i, idx + 1
                    buf[r_i]['timestamp'] = cnt
                    buf[r_i]['err'] = info | 0
        idx = idx + 1
    return r_i, t_i, idx


class InterRaw():
    def __init__(self, chunk=100000, debug=0):
        self.buf = np.empty(chunk, dtype=hit_dtype)
        self.buf_ts = np.empty(chunk, dtype=ts_dtype)
        self.buf_trig = np.empty(chunk, dtype=ts_dtype)
        self.r_i = 0
        self.state = 0
        self.buf[0]['err'] = 0xF
        self.t_i = -1
        self.chunk = chunk
        self.debug = debug

    def reset(self):
        if self.state == 1:
            self.t_i = -1
        elif self.state & 0x4 != 0:
            self.buf_ts[0] = self.buf_ts[self.t_i]
            self.t_i = 0

        if self.state & 0x2 != 0:
            info = np.uint(self.r_i & 0xF0)
            self.buf[0] = self.buf[-1]
            if self.buf[0]['err'] != 0:
                byte_stat = np.uint(0xF)
            else:
                byte_stat = np.uint(0)
            self.buf[0]['err'] = info | byte_stat
        else:
            self.buf[0] = self.buf[self.r_i]
        self.r_i = 0

        self.state = 0

    def run(self, raw, idx_offset=0):
        self.r_i, self.t_i, idx = _interpret(raw, self.buf, self.buf_ts, self.buf_trig, idx_offset=idx_offset)
        if len(raw) != idx:
            if self.t_i >= len(self.buf_ts):
                print('ts_buffer is full')
                self.state = 1
                return self.buf[:self.r_i], self.buf_ts, self.buf_trig[0:0], idx + idx_offset
            else:
                print('hit_buffer is full')
                if self.t_i != -1 and self.buf_ts[self.t_i]['err'] != 0:
                    print('imcomplete ts', self.buf_ts[self.t_i])
                    self.state = 4 + 2
                    return self.buf, self.buf_ts[:self.t_i], self.buf_trig[0:0], idx + idx_offset
                else:
                    self.state = 2
                    return self.buf, self.buf_ts[:self.t_i + 1], self.buf_trig[0:0], idx + idx_offset

        if (self.buf[self.r_i]['err'] & 0x0F) not in [0, 0xF]:
            print('imcomplete hit', self.buf[self.r_i]['err'] & 0x0F, self.buf[self.r_i])

        if self.t_i != -1 and self.buf_ts[self.t_i]['err'] != 0:
            print('imcomplete ts', self.buf_ts[self.t_i])
            self.state = 4
            return self.buf[:self.r_i], self.buf_ts[:self.t_i], self.buf_trig[0:0], idx + idx_offset
        else:
            # print('completed ts', self.buf_ts[self.t_i])
            self.state = 0
            return self.buf[:self.r_i], self.buf_ts[:self.t_i + 1], self.buf_trig[0:0], idx + idx_offset

    def mk_list(self, raw):
        dat = self.run(raw)
        self.reset()
        return dat


def interpret_h5(fin, fout, chunk=10_000_000):
    with tables.open_file(fout, "w") as f_o, tables.open_file(fin) as f_i:
        hit_table = f_o.create_table(f_o.root,
                                     name="hit_data",
                                     description=np.dtype(hit_dtype),
                                     title='hit_data')
        ts_table = f_o.create_table(f_o.root,
                                    name="ts_data",
                                    description=np.dtype(ts_dtype),
                                    title='ts_data')
        trig_table = f_o.create_table(f_o.root,
                                    name="trig_data",
                                    description=np.dtype(ts_dtype),
                                    title='trig_data')
        end = len(f_i.root.raw_data)
        start = 0
        inter = InterRaw(chunk=min(end, chunk // 2))
        t0 = time.time()
        while start < end:
            tmpend = min(end, start + chunk)
            hit, ts, trig, start = inter.run(f_i.root.raw_data[start:tmpend], idx_offset=start)
            hit_table.append(hit)
            hit_table.flush()
            ts_table.append(ts)
            ts_table.flush()
            print('{0:.2f}s {1:d}/{2:d} hit={3} ts={4}'.format(time.time()-t0, tmpend, end,
                                                               hit_table.shape[0], ts_table.shape[0],))
            inter.reset()


def raw2list(raw):
    inter = InterRaw(chunk=len(raw) + 10)
    hit, ts, trig, idx = inter.run(raw)
    if idx != len(raw):
        logger.warn('buffer was too small len_raw={0} interpreted-idx={0}'.format(len(raw),idx))
    return hit, ts, trig


if __name__ == "__main__":
    import sys 
    fin = sys.argv[1]
    fout = fin[:-3] + "_hit.h5"
    interpret_h5(fin, fout, debug=3)
