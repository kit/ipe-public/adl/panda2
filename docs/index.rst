.. Panda2 DAQ documentation master file, created by
   sphinx-quickstart on Mon May 22 13:25:54 2023.


Welcome to Panda2 DAQ's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   hw
   configuration
   digital



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
