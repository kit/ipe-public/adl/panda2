#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# ASIC and Detector Laboratory, IPE, KIT
# ------------------------------------------------------------
#

from basil.HL.RegisterHardwareLayer import RegisterHardwareLayer


class regs(RegisterHardwareLayer):
    '''
    '''
    _registers = {
          'VERSION': {'descr': {'addr': 0, 'size': 8, 'offset': 0, 'properties': ['ro']}},
          'READY_HIT': {'descr': {'addr': 1, 'size': 1, 'offset': 0, 'properties': ['ro']}},
          'CLK_HIT_GATE': {'descr': {'addr': 1, 'size': 1, 'offset': 1}},
          'RESET_HIT': {'descr': {'addr': 1, 'size': 1, 'offset': 2}},
          'GECCO_DAC1': {'descr': {'addr': 2, 'size': 14, 'offset': 0}},
          
          }

    _require_version = "==1"

    def __init__(self, intf, conf):
        super(regs, self).__init__(intf, conf)

    def get_READY_HIT(self):
        #print("=====sim=====regs.py get_READY")
        ret = self._intf.read_str(
            self._conf['base_addr']+self._registers['READY_HIT']['descr']['addr'], 
            size=1)
        return ret[0][7-self._registers['READY_HIT']['descr']['offset']]