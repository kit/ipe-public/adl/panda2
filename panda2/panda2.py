#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# ASIC and Detector Laboratory, IPE, KIT
# ------------------------------------------------------------
#

import os
import time
import logging
import yaml
import numpy as np

from basil.dut import Dut

import pkg_resources
VERSION = pkg_resources.get_distribution("panda2-daq").version
OUTPUT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + os.sep + 'output'


def mk_fname(ext="data.npy", dirname=None):
    if dirname is None:
        prefix = ext.split(".")[0]
        dirname = os.path.join(OUTPUT_DIR, prefix)
    if not os.path.exists(dirname):
        os.system("mkdir -p {0:s}".format(dirname))
    return os.path.join(dirname, time.strftime("%Y%m%d_%H%M%S_") + ext)

NCOLS = 29
NROWS = 62
NCOMPS = 2

class Panda2(Dut):

    def __init__(self, conf=None, **kwargs):

        # set logger
        self.logger = logging.getLogger()
        logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s] (%(threadName)-10s) %(message)s")
        fname = mk_fname(ext="panda2.log")
        fileHandler = logging.FileHandler(fname)
        fileHandler.setFormatter(logFormatter)
        self.logger.addHandler(fileHandler)
        self.conf_buf = []

        # prepare some constants and memories
        self.ncols = NCOLS
        self.nrows = NROWS
        self.ncomps = NCOMPS
        self.PixelConf = {'en_comp0': np.ones([self.ncols, self.nrows], dtype='u1') * 0xFF,
                          'tdac0': np.ones([self.ncols, self.nrows], dtype='u1') * 0xFF,
                          'en_comp1': np.ones([self.ncols, self.nrows], dtype='u1') * 0xFF,
                          'tdac1': np.ones([self.ncols, self.nrows], dtype='u1') * 0xFF
                          }

        if conf is None:
            conf = os.path.dirname(os.path.abspath(__file__)) + os.sep + "panda2.yml"
        if isinstance(conf, str):
            with open(conf) as f:
                conf = yaml.safe_load(f)  # TODO (TH) use safe_load
        super(Panda2, self).__init__(conf=conf)

    def init(self):
        super(Panda2, self).init()
        fw_version = self.get_fw_version()
        logging.info("Firmware version: {0}".format(fw_version))
        self['INJ_BOARD'].set_size(self["INJ_BOARD"]._conf['size'])
        self['CONF'].set_size(self["CONF"]._conf['size'])
        self.reset_chip()
        # set default values for sync pulsers
        self['sync'].WIDTH = 1
        self['sync'].DELAY = 2**24 - 1
        self['sync'].REPEAT = 0
        self['gate'].REPEAT = 1
        self['gate'].WIDTH = self['sync'].WIDTH + self['sync'].DELAY
        self['gate'].DELAY = 1

    def reconnect(self):
        try:
            self['intf'].close()
        except Exception:
            self.logger.warn('reconnect: port close failed')
        self['intf'].init()

    def get_fw_version(self):
        return self['intf'].read(0x10000, 1)[0]

    def reset_chip(self):
        # Rst chip
        self['SW']['RstAnalogB'] = 0
        self['SW'].write()
        self['SW']['RstAnalogB'] = 1
        self['SW'].write()
        # Initialize CONF
        self['CONF'].set_configuration(self['CONF']._init)
        for c in range(self.ncols):
            self['CONF']['PixelConf'][c]['WrRAM'] = 0x1F
            self['CONF']['PixelConf'][c]['en_comp'] = 0x0
            self['CONF']['PixelConf'][c]['TuneDAC'] = 0x7
        self.PixelConf['tdac0'][:, :] = 0x7
        self.PixelConf['en_comp0'][:, :] = 0x0
        self.PixelConf['tdac1'][:, :] = 0x7
        self.PixelConf['en_comp1'][:, :] = 0x0
        # Write RAM
        self['CONF'].write()
        for i in range(1000):
            time.sleep(0.002)
            if self['CONF'].is_ready:
                break
        for c in range(self.ncols):
            self['CONF']['PixelConf'][c]['WrRAM'] = 0x0
        self['CONF'].write()
        for i in range(1000):
            time.sleep(0.002)
            if self['CONF'].is_ready:
                break
        self.logger.info('reset_chip: init conf and pixel-RAM')

    def set_conf(self, init=False, write=True, **kwargs):
        self.conf_buf.append('set_conf:')
        if init:
            self['CONF'][:] = 0
            self['CONF'].set_configuration(self['CONF']._init)
            self.conf_buf[-1] = self.conf_buf + 'init'
        for k in kwargs:
            self['CONF'][k] = kwargs[k]
            self.conf_buf[-1] = self.conf_buf[-1] + '{}={}'.format(k, kwargs[k])
        if write:
            self._write_conf(en_comp0=None, tdac0=None, en_comp1=None, tdac1=None)

    def set_inj(self, inj_n=100, inj_width=10240, inj_delay=10240, inj_phase=-1, ext=False):#ivan changed from 100 to 20
        self["inj"].reset()
        self["inj"]["REPEAT"] = inj_n
        self["inj"]["DELAY"] = inj_delay
        self["inj"]["WIDTH"] = inj_width
        self["inj"]["EN"] = ext
        self['gate'].REPEAT = 1
        self['gate'].WIDTH = self['sync'].WIDTH + self['sync'].DELAY
        self['gate'].DELAY = 1
        if ext:
            self['sync'].start()
        inj_phase_des = 0x0
        for i in range(8):  # CLKDV *4 = 2*4 = 8
            if i > inj_phase:
                inj_phase_des = (inj_phase_des << 1) | 1
        self["inj"]["PHASE_DES"] = inj_phase_des

        self.conf_buf.append("set_inj: inj_width={0:d} inj_delay={1:d} inj_phase={2:d} inj_n={3:d} ext={4:d}".format(
            inj_width, inj_delay, inj_phase, inj_n, int(ext)))

    def set_en_ampout(self, cols='none', write=True):
        if isinstance(cols, int):
            cols = [cols]
        elif isinstance(cols, str):  # if 'none', disable all
            cols = []
        for col in range(self.ncols):
            if col in cols:
                self['CONF']['PixelConf'][self.ncols - 1 - col]['en_ampout_col'] = 1
            else:
                self['CONF']['PixelConf'][self.ncols - 1 - col]['en_ampout_col'] = 0
        # log
        en_ampout_col = 0
        for c in range(self.ncols):
            en_ampout_col = (en_ampout_col << 1) | self['CONF']['PixelConf'][c]['en_ampout_col'].tovalue()
        self.conf_buf.append('set_en_ampout: en_ampout_col=0x{0:08x}'.format(en_ampout_col))
        if write:
            self._write_conf(en_comp0=None, tdac0=None, en_comp1=None, tdac1=None)

    def set_en_inj(self, pix='none', cols=None, rows=None, write=True):
        if rows is not None:
            rows = np.unique(rows)
        if cols is not None:
            cols = np.unique(cols)
        if cols is None and rows is None:
            if isinstance(pix, str):
                if pix == 'all':
                    cols = range(self.ncols)
                    rows = range(12)
                elif pix == 'none':
                    cols = []
                    rows = []
            elif isinstance(pix[0], int):
                cols = np.array([pix[0]])
                rows = np.array([pix[1]])
            elif len(pix[0]) == 2:
                pix = np.array(pix)
                cols = np.unique(pix[:, 0])
                rows = np.unique(pix[:, 1])

        if cols is not None:
            for col in range(self.ncols):
                if col in cols:
                    self['CONF']['PixelConf'][self.ncols - 1 - col]['en_injection_col'] = 1
                else:
                    self['CONF']['PixelConf'][self.ncols - 1 - col]['en_injection_col'] = 0
        if rows is not None:
            for row in range(self.nrows):
                if row in rows:
                    val = True  # '1'
                else:
                    val = False  # '0'
                div_r, mod_r = divmod(row * 2, 5)
                if (div_r % 2):  # odd
                    self['CONF']['PixelConf'][self.ncols - 1 - div_r]['en_injection_row'][4 - mod_r] = val
                else:  # even
                    self['CONF']['PixelConf'][self.ncols - 1 - div_r]['en_injection_row'][mod_r] = val
        # log
        en_injection_col = 0
        en_injection_row0 = ''
        en_injection_row1 = ''
        for c in range(self.ncols):
            en_injection_col = (en_injection_col << 1) | self['CONF']['PixelConf'][c]['en_injection_col'].tovalue()
            if c % 2:
                en_injection_row0 = en_injection_row0 + '_' + self['CONF']['PixelConf'][c]['en_injection_row'].to01()
            else:
                en_injection_row1 = en_injection_row1 + '_' + self['CONF']['PixelConf'][c]['en_injection_row'].to01()

        self.conf_buf.append('set_en_inj: en_injection_col=0x{0:08x} en_injection_row0={1} en_injection_row1={2}'.format(
            en_injection_col, en_injection_row0[1:], en_injection_row1[1:]))
        if write:
            self._write_conf(en_comp0=None, tdac0=None, en_comp1=None, tdac1=None)

    def get_en_inj(self):
        cols = []
        rows = []
        for col in range(self.ncols):
            if self['CONF']['PixelConf'][self.ncols - 1 - col]['en_injection_col'].tovalue() == 1:
                cols.append(col)
        for row in range(self.nrows):
            div_r, mod_r = divmod(row * 2, 5)
            if (div_r % 2):  # odd
                val = int(self['CONF']['PixelConf'][self.ncols - 1 - div_r]['en_injection_row'][4 - mod_r])
            else:  # even
                val = int(self['CONF']['PixelConf'][self.ncols - 1 - div_r]['en_injection_row'][mod_r])
            if val:
                rows.append(row)
        return cols, rows

    def set_en_hitbus(self, cols, write=True):
        if isinstance(cols, int):
            cols = [cols]
        elif isinstance(cols, str):
            if cols == 'none':
                cols = []
            else:   # if 'all', enable all
                cols = range(self.ncols)
        for col in range(self.ncols):
            if col in cols:
                self['CONF']['PixelConf'][self.ncols - 1 - col]['en_hitbus_col'] = False
            else:
                self['CONF']['PixelConf'][self.ncols - 1 - col]['en_hitbus_col'] = True
        # log
        en_hitbus_col = 0
        for c in range(self.ncols):
            en_hitbus_col = (en_hitbus_col << 1) | self['CONF']['PixelConf'][c]['en_hitbus_col'].tovalue()
        self.conf_buf.append('set_en_hitbus: en_hitbus_col=0x{0:08x}'.format(en_hitbus_col))
        if write:
            self._write_conf(en_comp0=None, tdac0=None, en_comp1=None, tdac1=None)

    def set_en_comp(self, pix, comp='both', write=True):
        if isinstance(comp, str):
            if comp == 'both':
                comp = 3
            elif comp == 'fast':
                comp = 0
            elif comp == 'slow':
                comp = 1
        if comp == 0:
            en_comp = np.copy(self.PixelConf['en_comp0'][:, :]) ##fast
        elif comp == 1:
            en_comp = np.copy(self.PixelConf['en_comp1'][:, :]) ##slow
        elif comp == 3:  ##both
            en_comp = self.PixelConf['en_comp0'][:, :] * self.PixelConf['en_comp1'][:, :]  # TODO this is not correct

        if isinstance(pix, str):
            if pix == 'all':
                en_comp[:, :] = 1
            elif pix == 'none':
                en_comp[:, :] = 0
        elif isinstance(pix[0], int):
            en_comp[:, :] = 0
            en_comp[pix[0], pix[1]] = 1
        elif len(pix[0]) == 2:
            en_comp[:, :] = 0
            for p in pix:
                en_comp[p[0], p[1]] = 1
        else:
            en_comp[:, :] = pix

        if write:
            if comp == 0:
                self._write_conf(en_comp0=en_comp, tdac0=None, en_comp1=None, tdac1=None)
            elif comp == 1:
                self._write_conf(en_comp0=None, tdac0=None, en_comp1=en_comp, tdac1=None)
            else:  # 'both'
                self._write_conf(en_comp0=en_comp, tdac0=None, en_comp1=en_comp, tdac1=None)

    def set_tdac(self, tdac, comp='fast'):
        if isinstance(tdac, int):
            tdac = np.ones(self.PixelConf['tdac0'].shape, dtype=np.uint8) * tdac
        if len(tdac.shape) == 3:
            self._write_conf(en_comp0=None, tdac0=tdac[0, :, :], en_comp1=None, tdac1=tdac[1, :, :])
        elif comp == 'fast':
            self._write_conf(en_comp0=None, tdac0=tdac, en_comp1=None, tdac1=None)
        elif comp == 'slow':
            self._write_conf(en_comp0=None, tdac0=None, en_comp1=None, tdac1=tdac)
        elif comp == 'both':
            self._write_conf(en_comp0=None, tdac0=tdac, en_comp1=None, tdac1=tdac)
        else:
            self.logger.warn("ERROR ERROR ERROR, comp= fast, slow or both")

    def set_ram(self, en_comp, tdac, comp='fast'):
        if len(en_comp.shape) == 3:
            self._write_conf(en_comp0=en_comp[:,:,0], tdac0=tdac[:,:,0], en_comp1=en_comp[:,:,1], tdac1=tdac[:,:,1])
        elif comp == 'fast':
            self._write_conf(en_comp0=en_comp, tdac0=tdac, en_comp1=None, tdac1=None)
        elif comp == 'slow':
            self._write_conf(en_comp0=None, tdac0=None, en_comp1=en_comp, tdac1=tdac)
        elif comp == 'both':
            self._write_conf(en_comp0=en_comp, tdac0=tdac, en_comp1=en_comp, tdac1=tdac)
        else:
            self.logger.warn("ERROR ERROR ERROR, comp= fast, slow or both")

    def _write_conf(self, en_comp0, tdac0, en_comp1, tdac1):
        # #################
        # Comp0 fast
        # find changed row
        rows = np.ones(0, dtype=int)
        if en_comp0 is not None:
            rows = np.argwhere(self.PixelConf['en_comp0'][:, :] != en_comp0)[:, 1]
        if tdac0 is not None:
            rows = np.append(rows, np.argwhere(self.PixelConf['tdac0'][:, :] != tdac0)[:, 1])
        rows = np.unique(rows)
        # RAM write
        for r in rows:
            div_r, mod_r = divmod(r * 2, 5)
            for c in range(self.ncols):
                if tdac0 is not None:
                    self['CONF']['PixelConf'][self.ncols - 1 - c]['TuneDAC'] = int(tdac0[c, r])
                    self.PixelConf['tdac0'][c, r] = int(tdac0[c, r])
                else:
                    tmp_tdac = int(min(self.PixelConf['tdac0'][c, r], 7))
                    self['CONF']['PixelConf'][self.ncols - 1 - c]['TuneDAC'] = tmp_tdac
                    self.PixelConf['tdac0'][c, r] = tmp_tdac
                if en_comp0 is not None:
                    self['CONF']['PixelConf'][self.ncols - 1 - c]['en_comp'] = int(en_comp0[c, r])
                    self.PixelConf['en_comp0'][c, r] = int(en_comp0[c, r])
                else:
                    tmp_comp = int(min(self.PixelConf['en_comp0'][c, r], 1))
                    self['CONF']['PixelConf'][self.ncols - 1 - c]['en_comp'] = tmp_comp
                    self.PixelConf['en_comp0'][c, r] = tmp_comp
            if (div_r % 2):  # even
                self['CONF']['PixelConf'][self.ncols - 1 - div_r]['WrRAM'][4 - mod_r] = '1'
            else:  # odd
                self['CONF']['PixelConf'][self.ncols - 1 - div_r]['WrRAM'][mod_r] = '1'
            self['CONF'].write()
            for i in range(1000):
                time.sleep(0.002)
                if self['CONF'].is_ready:
                    break
            for c in range(self.ncols):
                self['CONF']['PixelConf'][self.ncols - 1 - c]['WrRAM'] = 0
            self['CONF'].write()
            for i in range(1000):
                time.sleep(0.002)
                if self['CONF'].is_ready:
                    break
            self.logger.info('_write_conf row{} en_comp0{} tdac0{}'.format(r, self.PixelConf['en_comp0'][:, r], self.PixelConf['tdac0'][:, r]))

        # #################
        # Comp1 slow
        rows2 = np.ones(0, dtype=int)
        if en_comp1 is not None:
            rows2 = np.argwhere(self.PixelConf['en_comp1'][:, :] != en_comp1)[:, 1]
        if tdac1 is not None:
            rows2 = np.append(rows2, np.argwhere(self.PixelConf['tdac1'][:, :] != tdac1)[:, 1])
        rows2 = np.unique(rows2)

        # RAM write
        for r in rows2:
            div_r, mod_r = divmod(r * 2 + 1, 5)
            for c in range(self.ncols):
                if tdac1 is not None:
                    self['CONF']['PixelConf'][self.ncols - 1 - c]['TuneDAC'] = int(tdac1[c, r])
                    self.PixelConf['tdac1'][c, r] = int(tdac1[c, r])
                else:
                    tmp_tdac = int(min(self.PixelConf['tdac1'][c, r], 7))
                    self['CONF']['PixelConf'][self.ncols - 1 - c]['TuneDAC'] = tmp_tdac
                    self.PixelConf['tdac1'][c, r] = tmp_tdac
                if en_comp1 is not None:
                    self['CONF']['PixelConf'][self.ncols - 1 - c]['en_comp'] = int(en_comp1[c, r])
                    self.PixelConf['en_comp1'][c, r] = int(en_comp1[c, r])
                else:
                    tmp_comp = int(min(self.PixelConf['en_comp1'][c, r], 1))
                    self['CONF']['PixelConf'][self.ncols - 1 - c]['en_comp'] = tmp_comp
                    self.PixelConf['en_comp1'][c, r] = tmp_comp
            if (div_r % 2):  # even
                self['CONF']['PixelConf'][self.ncols - 1 - div_r]['WrRAM'][4 - mod_r] = '1'
            else:  # odd
                self['CONF']['PixelConf'][self.ncols - 1 - div_r]['WrRAM'][mod_r] = '1'
            self['CONF'].write()
            for i in range(1000):
                time.sleep(0.002)
                if self['CONF'].is_ready:
                    break
            for c in range(self.ncols):
                self['CONF']['PixelConf'][self.ncols - 1 - c]['WrRAM'] = 0
            self['CONF'].write()
            for i in range(1000):
                time.sleep(0.002)
                if self['CONF'].is_ready:
                    break
            self.logger.info('_write_conf row{} en_comp1{} tdac1{}'.format(r, self.PixelConf['en_comp1'][:, r], self.PixelConf['tdac1'][:, r]))

        # without RAM write (just update global shift reg)
        if len(rows) == 0 and len(rows2) == 0 and len(self.conf_buf) != 0:
            for c in range(self.ncols):
                self['CONF']['PixelConf'][self.ncols - 1 - c]['WrRAM'] = 0
            self['CONF'].write()
            for i in range(1000):
                time.sleep(0.002)
                if self['CONF'].is_ready:
                    break
        for lb in self.conf_buf:
            self.logger.info(lb)
        self.conf_buf = []

    def convert_inj_DAU2V(self, dau):
        a, b, c = (1.6750507793825382e-08, 0.00013919799450240716, 0.013104904296381592)
        return dau * dau * a + b * dau + c

    def convert_inj_V2DAC(self, vol):
        a, b, c = (1.6750507793825382e-08, 0.00013919799450240716, 0.013104904296381592)
        return (np.sqrt(b * b - 4 * a * (c - vol)) - b) / (2 * a)

    def set_inj_amp(self, inj_amp, unit='V'):
        if unit == 'V':
            # VDD3V3 = 3V
            a, b, c = (1.6750507793825382e-08, 0.00013919799450240716, 0.013104904296381592)
            inj_amp = (np.sqrt(b * b - 4 * a * (c - inj_amp)) - b) / (2 * a)
        inj_amp = int(inj_amp)
        if inj_amp > 9830:
            self.logger.warn('set_inj_amp: fail to set inj_amp={0:d}DAU, inj_amp must be 0-1.8V(9830DAU)'.format(inj_amp))
            inj_amp = 9830
        elif inj_amp < 0:
            self.logger.warn('set_inj_amp: fail to set inj_amp={0:d}DAU, inj_amp must be 0-1.8V(9830DAU)'.format(inj_amp))
            inj_amp = 0
        self['INJ_BOARD']['AMP'] = inj_amp
        self['INJ_BOARD']['LOAD'] = 0
        self['INJ_BOARD'].write()
        for i in range(1000):
            if self['INJ_BOARD'].is_ready:
                break
            else:
                time.sleep(0.001)
        self['INJ_BOARD']['LOAD'] = 1
        self['INJ_BOARD'].write()
        while not self['INJ_BOARD'].is_ready:
            time.sleep(0.001)

        self.logger.info("inj_amp: {0}".format(inj_amp))

    def inject(self, wait=False):
        if self["inj"].EN:
            self["gate"].start()
            while (not self["gate"].is_ready) and wait:
                time.sleep(0.001)
        else:
            self["inj"].start()
            while (not self["inj"].is_ready) and wait:
                time.sleep(0.001)

    def get_data_now(self):
        return self['fifo'].get_data()

    def get_data(self, timeout=1, n_ts=None):
        # start injection
        self['data_rx'].set_en(True)
        if self["inj"].EN:
            self["gate"].start()
        else:
            self["inj"].start()
        if n_ts is None:
            n_ts = self['inj'].REPEAT
        nloops = int(timeout // 0.01)

        # get data
        raw = self['fifo'].get_data()
        for i in range(nloops):
            time.sleep(0.01)
            raw = np.append(raw, self['fifo'].get_data())
            if n_ts != 0 or self["inj"].is_ready:
                ts_len = (raw & 0xF000_0000 == 0x8000_0000).sum()
                break
        for j in range(i, nloops):
            time.sleep(0.01)
            tmp_raw = self['fifo'].get_data()
            raw = np.append(raw, tmp_raw)
            if n_ts != 0:
                ts_len = ts_len + (tmp_raw & 0xF000_0000 == 0x8000_0000).sum()
                # print(i, ts_len, n_ts)
                if ts_len >= n_ts * 3:
                    break
        time.sleep(0.01)
        raw = np.append(raw, self['fifo'].get_data())

        # set rx off
        self['data_rx'].set_en(False)
        time.sleep(0.01)
        raw = np.append(raw, self['fifo'].get_data())

        # check status
        rx_status = self["data_rx"].STATUS
        fifo_size = self['fifo'].FIFO_SIZE
        if rx_status & 0x8 == 0x0:
            rx_status = self["data_rx"].STATUS
        if rx_status != 0x8 or fifo_size != 0 or j == nloops - 1:
            self.logger.warn("get_data: error status=0x{0:x} fifo_size={1:d} loop={2:d} ts={3}/{4}".format(
                rx_status, fifo_size, j, ts_len, n_ts * 3))
        return raw

    def get_rx_status(self):
        self['data_rx'].RAW_DATA_WR = True
        self['data_rx'].RAW_DATA_WR = False
        ret = {'raw0': self['data_rx'].RAW_DATA0,
               'raw1': self['data_rx'].RAW_DATA1,
               'aligned': self['data_rx'].ALIGNED,
               'lost_cnt': self["data_rx"].LOST_COUNT,
               'decoder_err_cnt': self["data_rx"].DECODER_ERR_CNT,
               'data_err_cnt': self["data_rx"].DATA_ERR_CNT
               }
        ret['ts_inj_lost_cnt'] = self["ts_inj"].LOST_COUNT
        ret['sync_ready'] = self['sync'].is_ready
        return ret

    def get_sync_status(self):
        self.logger.info('get_inj_status: sw={}'.format(self['SW'].get_configuration()))
        self.logger.info('get_inj_status: sync={} is_ready={}'.format(self['sync'].get_configuration(), self['sync'].is_ready))
        self.logger.info('get_inj_status: inj ={} is_ready={}'.format(self['inj'].get_configuration(), self['inj'].is_ready))
        self.logger.info('get_inj_status: gate={} is_ready={}'.format(self['gate'].get_configuration(), self['gate'].is_ready))

    def set_sync(self, nbits=24, force_reset_time=0.1, auto_sync=True):
        ''' this function does:
               1. sends a sync-reset manually
               2. set automatic sync-reset and starts it
            nbits(int) : number of bits to sync
            wait(float): pluse length (sec) of the manual sync-reset, if negative, sync-reset kept HIGH
            example:
        '''
        # send manula sync
        if force_reset_time != 0:
            self['SW']['SYNC_RES'] = 1
        self['SW']['EN_PULSE_SYNC'] = 0
        self['SW'].write()
        # configure automatic reset
        self['sync'].REPEAT = 0
        self['sync'].DELAY = 2**nbits - 1
        self['sync'].WIDTH = 1
        self['sync'].start()
        if force_reset_time > 0:
            time.sleep(force_reset_time)
            self['SW']['EN_PULSE_SYNC'] = auto_sync
            self['SW']['SYNC_RES'] = 0
            self['SW'].write()
        self['sync'].start()

        self.logger.info('set_sync: sync_period=0x{0:x} repeat={1} auto-sync={2} manual-sync={3}'.format(
            self['sync'].DELAY + self['sync'].WIDTH,
            self['sync'].REPEAT,
            self['SW']['EN_PULSE_SYNC'],
            self['SW']['SYNC_RES']))

    def set_rx(self, en=True, reset_wait=0.1):
        if en:
            # self['SW']['EN_CkExt'] = False
            # self['SW']['SYNC_RES'] = 1
            # self['SW'].write()
            self['SW']['EN_CkExt'] = True
            self['SW']['SYNC_RES'] = 1
            self['SW'].write()

            self['data_rx'].reset()
            if self['fifo'].FIFO_SIZE != 0:  # Clear fifo
                self.logger.info('set_rx: FIFO is not empty({}), try to discard the data...'.format(self['fifo'].FIFO_SIZE))
                self['fifo'].get_data()
                if self['fifo'].FIFO_SIZE != 0:
                    self.logger.warn('set_rx: failed to empty FIFO({0})'.format(self['fifo'].FIFO_SIZE))
            cnt0 = int(reset_wait / 0.010)
            cnt = 0
            for i in range(cnt0 * 10):  # aligne data
                time.sleep(0.010)
                if self['data_rx'].ALIGNED:
                    cnt = cnt + 1
                if cnt > cnt0:
                    break
            if i == cnt0 * 10 - 1:
                self.logger.warn('set_rx: FPGA cannot align data: aligned={0}'.format(self['data_rx'].ALIGNED))
            self['SW']['SYNC_RES'] = 0
            self['SW'].write()
            self['sync'].START = 1
            #self['data_rx'].set_en(en)
            aligned = self['data_rx'].ALIGNED
            self.logger.info('set_rx: aligned={0}'.format(aligned))
        else:
            self['SW']['EN_CkExt'] = False
            self['SW']['SYNC_RES'] = 1
            self['SW'].write()
            self['data_rx'].set_en(False)
            self.logger.info('set_rx: stopped')
        return aligned

    def mask_rx(self, mask=True):
        self['data_rx'].set_en(not mask)
        if not mask:
            trash = self['fifo'].FIFO_SIZE
            if trash != 0:
                self['fifo'].get_data()
                self.logger.warn('mask_rx: discard trash ({0} {1})'.format(trash, self['fifo'].FIFO_SIZE))

    def set_ts_inj(self, en=True, reset=False):
        if en:
            self['ts_inj'].RESET = reset
        self['ts_inj'].ENABLE_RISING = en

    def get_configuration(self):
        ret = super(Panda2, self).get_configuration()
        ret['PixelConf'] = self.PixelConf
        # add some statuses
        ret['status'] = self.get_rx_status()
        ret['status']['inj_ready'] = self['inj'].READY
        ret['status']['sync_ready'] = self['sync'].READY
        ret['status']['gate_ready'] = self['gate'].READY
        ret['status']['timestamp'] = self['ts_inj'].TIMESTAMP
        return ret

if __name__ == '__main__':
    pass
