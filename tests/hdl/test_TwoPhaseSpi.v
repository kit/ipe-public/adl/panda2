/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */

`timescale 1ps / 1ps

`include "utils/bus_to_ip.v"
`include "gpio/gpio_core.v"
`include "gpio/gpio.v"

`include "two_phase_spi/two_phase_spi.v"
`include "two_phase_spi/two_phase_spi_core.v"
`include "spi/blk_mem_gen_8_to_1_2k.v"

`include "utils/cdc_syncfifo.v"
`include "utils/generic_fifo.v"
`include "utils/cdc_pulse_sync.v"
`include "utils/CG_MOD_pos.v"
`include "utils/clock_divider.v"
`include "utils/3_stage_synchronizer.v"
`include "utils/RAMB16_S1_S9_sim.v"


module tb (
    input wire FCLK_IN,

    inout wire [7:0] BUS_DATA,
    input wire [15:0] ADD,
    input wire RD_B,
    input wire WR_B
);

// MODULE ADREESSES //
localparam TWO_PHASE_SPI_BASEADDR = 16'h100;     //0x1000
localparam TWO_PHASE_SPI_HIGHADDR = 16'h200-1;   //0x300f

localparam PULSE_BASEADDR = 16'h200;
localparam PULSE_HIGHADDR = PULSE_BASEADDR + 15;

localparam ABUSWIDTH = 16;

wire SPI_CLK;
wire EX_START;
wire [7:0] IO;
gpio#(
    .BASEADDR(PULSE_BASEADDR),
    .HIGHADDR(PULSE_HIGHADDR),
    .ABUSWIDTH(ABUSWIDTH),
    .IO_WIDTH(8),
    .IO_DIRECTION(8'hFF)
) i_gpio (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .IO(IO)
);
assign EXT_START = IO[0];

clock_divider #(
    .DIVISOR(4)
) i_clock_divisor (
    .CLK(BUS_CLK),
    .RESET(1'b0),
    .CE(),
    .CLOCK(SPI_CLK)
);

wire SCLK, SDI, SDO, SEN, SLD, SRB;

two_phase_spi #(
    .BASEADDR(TWO_PHASE_SPI_BASEADDR),
    .HIGHADDR(TWO_PHASE_SPI_HIGHADDR),
    .ABUSWIDTH(ABUSWIDTH),
    .MEM_BYTES(5)
) i_two_phase_spi (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .SPI_CLK(SPI_CLK),
    .EXT_START(EXT_START),

    .SCLK1(SCLK1),
    .SCLK2(SCLK2),
    .SDI(SDI),
    .SDO(SDO),
    .SRB(SRB),
    .SLD(SLD),

    .SEN(SEN)
);

assign SDO = SDI;

`ifndef VERILATOR_SIM
initial begin
    $dumpfile("spi.vcd");
    $dumpvars(0);
end
`endif

endmodule
