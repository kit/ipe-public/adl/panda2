#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# ASIC and Detector Laboratory, IPE, KIT
# ------------------------------------------------------------
# The original file is from https://github.com/SiLab-Bonn/basil
# Initial version by Chris Higgs <chris.higgs@potentialventures.com>
#

# pylint: disable=pointless-statement, expression-not-assigned

"""
Abastract away interactions with the control bus
"""
from __future__ import print_function
import cocotb
from cocotb.binary import BinaryValue
from cocotb.triggers import RisingEdge, ReadOnly, Timer
from cocotb_bus.drivers import BusDriver
from cocotb.result import ReturnValue
from cocotb.clock import Clock


class Panda2UsbBusDriver(BusDriver):

    _signals = ["FCLK_IN", "BUS_DATA", "ADD", "RD_B", "WR_B", "FD", "FREAD", "FSTROBE", "FMODE"]

    BASE_ADDRESS_I2C = 0x00000
    HIGH_ADDRESS_I2C = BASE_ADDRESS_I2C + 256

    BASE_ADDRESS_EXTERNAL = 0x10000
    HIGH_ADDRESS_EXTERNAL = 0x10000 + 0x10000

    BASE_ADDRESS_BLOCK = 0x0001000000000000
    HIGH_ADDRESS_BLOCK = 0xffffffffffffffff

    n_clk = 5

    def __init__(self, entity):
        BusDriver.__init__(self, entity, "", entity.FCLK_IN)

        # Create an appropriately sized high-impedence value
        self._high_impedence = BinaryValue(n_bits=len(self.bus.BUS_DATA))
        self._high_impedence.binstr = "Z" * len(self.bus.BUS_DATA)

        # Create an appropriately sized high-impedence value
        self._x = BinaryValue(n_bits=16)
        self._x.binstr = "x" * 16

        # Kick off a clock generator
        cocotb.start_soon(Clock(self.clock, 20800).start())

    @cocotb.coroutine
    def init(self):
        # Defaults
        # self.bus.BUS_RST<= 1
        self.bus.RD_B.value = 1
        self.bus.WR_B.value = 1
        self.bus.ADD.value = 0
        self.bus.FREAD.value = 0
        self.bus.FSTROBE.value = 0
        self.bus.FMODE.value = 0
        self.bus.BUS_DATA.value = self._high_impedence
        self.bus.FD.value = self._high_impedence

        # wait for reset
        for _ in range(200):
            yield RisingEdge(self.clock)

    @cocotb.coroutine
    def read(self, address, size, dtype="u"):
        result = []
        if address >= self.BASE_ADDRESS_I2C and address < self.HIGH_ADDRESS_I2C:
            self.entity._log.warning("I2C address space supported in simulation!")
            for byte in range(size):
                result.append(0)
        elif (address >= self.BASE_ADDRESS_EXTERNAL and address < self.HIGH_ADDRESS_EXTERNAL):
            for byte in range(size):
                val = yield self.read_external(address - self.BASE_ADDRESS_EXTERNAL + byte, dtype)
                result.append(val)
        elif (address >= self.BASE_ADDRESS_BLOCK and address < self.HIGH_ADDRESS_BLOCK):
            #print("block",size)
            for byte in range(size):
                val = yield self.fast_block_read(dtype)
                result.append(val)
        else:
            self.entity._log.warning("This address space does not exist!")
        return result

    @cocotb.coroutine
    def write(self, address, data):
        if address >= self.BASE_ADDRESS_I2C and address < self.HIGH_ADDRESS_I2C:
            self.entity._log.warning("I2C address space supported in simulation!")
        elif address >= self.BASE_ADDRESS_EXTERNAL and address < self.HIGH_ADDRESS_EXTERNAL:
            for index, byte in enumerate(data):
                yield self.write_external(address - self.BASE_ADDRESS_EXTERNAL + index, byte)
        elif(address >= self.BASE_ADDRESS_BLOCK and address < self.HIGH_ADDRESS_BLOCK):
            raise NotImplementedError("Unsupported request")
            # self._sidev.FastBlockWrite(data)
        else:
            self.entity._log.warning("This address space does not exist!")

    @cocotb.coroutine
    def read_external(self, address, dtype):
        """Copied from silusb.sv testbench interface"""
        self.bus.RD_B.value = 1
        self.bus.ADD.value = self._x
        self.bus.BUS_DATA.value = self._high_impedence
        for _ in range(self.n_clk):
            yield RisingEdge(self.clock)

        yield RisingEdge(self.clock)
        self.bus.ADD.value = address + 0x4000

        yield RisingEdge(self.clock)
        self.bus.RD_B.value = 0
        yield RisingEdge(self.clock)
        self.bus.RD_B.value = 0
        yield ReadOnly()
        if dtype == "u":
            try:
                result = self.bus.BUS_DATA.value.integer
            except:
                print(hex(address),self.bus.BUS_DATA.value.binstr)
        else:
            result = self.bus.BUS_DATA.value.binstr
        yield RisingEdge(self.clock)
        self.bus.RD_B.value = 1

        yield RisingEdge(self.clock)
        self.bus.RD_B.value = 1
        self.bus.ADD.value = self._x

        yield RisingEdge(self.clock)

        for _ in range(self.n_clk):
            yield RisingEdge(self.clock)

        #raise ReturnValue(result)
        return result

    @cocotb.coroutine
    def write_external(self, address, value):
        """Copied from silusb.sv testbench interface"""
        self.bus.WR_B.value = 1
        self.bus.ADD.value = self._x

        for _ in range(self.n_clk):
            yield RisingEdge(self.clock)

        yield RisingEdge(self.clock)
        self.bus.ADD.value = address + 0x4000
        self.bus.BUS_DATA.value = int(value)
        yield Timer(1)  # This is hack for iverilog
        self.bus.ADD.value = address + 0x4000
        self.bus.BUS_DATA.value = int(value)
        yield RisingEdge(self.clock)
        self.bus.WR_B.value = 0
        yield Timer(1)  # This is hack for iverilog
        self.bus.BUS_DATA.value = int(value)
        self.bus.WR_B.value = 0
        yield RisingEdge(self.clock)
        self.bus.WR_B.value = 0
        yield Timer(1)  # This is hack for iverilog
        self.bus.BUS_DATA.value = int(value)
        self.bus.WR_B.value = 0
        yield RisingEdge(self.clock)
        self.bus.WR_B.value = 1
        self.bus.BUS_DATA.value = self._high_impedence
        yield Timer(1)  # This is hack for iverilog
        self.bus.WR_B.value = 1
        self.bus.BUS_DATA.value = self._high_impedence
        yield RisingEdge(self.clock)
        self.bus.WR_B.value = 1
        self.bus.ADD.value = self._x

        for _ in range(self.n_clk):
            yield RisingEdge(self.clock)

    @cocotb.coroutine
    def fast_block_read(self, dtype):
        yield RisingEdge(self.clock)
        self.bus.FREAD.value = 1
        self.bus.FSTROBE.value = 1
        yield ReadOnly()
        if dtype == "u":
            try:
                result = self.bus.FD.value.integer
            except:
                print('block',self.bus.FD.value.binstr)
        else:
            result = self.bus.FD.value.binstr
        yield RisingEdge(self.clock)
        self.bus.FREAD.value = 0
        self.bus.FSTROBE.value = 0
        yield RisingEdge(self.clock)
        return result
