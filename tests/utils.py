#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# ASIC and Detector Laboratory, IPE, KIT
# ------------------------------------------------------------
#

import os
import yaml
import numpy as np
from basil.utils.sim.utils import cocotb_compile_and_run


def sim_setup(extra_defines=[],
              conf="panda2/panda2_sim.yaml", 
              sim_file='tests/hdl/tb.v', hit_file=None, sim='icarus'):
    os.environ["SIM"] = sim  # icarus, questa, xcelium
    if sim == "xcelium":
        extra = "EXTRA_ARGS = -sv"
    elif sim == 'questa':
        extra = "EXTRA_ARGS = -g2012"
    else:
        extra = ''
    if hit_file is not None and len(hit_file) > 0:
        os.environ['SIMULATION_MODULES'] = yaml.dump({'panda2.sim.HitDataFile': {
                'filename': hit_file
            }})
    root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sim_files = [os.path.join(root_dir, sim_file)]

    cocotb_compile_and_run(
        test_module='panda2.sim.Test',
        sim_files=sim_files,
        extra_defines=extra_defines,
        sim_bus="panda2.sim.Panda2UsbBusDriver",
        include_dirs=(root_dir,
                      root_dir + "/firmware/src",
                      root_dir + "/chip",
                      root_dir + "/chip/building_blocks/spi/inc",
                      root_dir + "/chip/building_blocks/shiftregister/inc",
                      root_dir + "/chip/building_blocks/spi/commander/inc",
                      root_dir + "/chip/building_blocks/fifo/inc",
                      root_dir + "/chip/pixelmatrix_model",
                      os.environ["AMS_HOME"]
                      ),
        extra=extra
    )

    if conf is not None:
        with open(root_dir + conf, "r") as f:
            conf = yaml.safe_load(f)
        conf["transfer_layer"][0]["type"] = "panda2.sim.SiSim"

    return conf

def startHIT(dut, fname):
    dut['regs'].RESET_HIT = 1
    dut['regs'].CLK_HIT_GATE = 1
    for i in range(100):
        start = dut['regs'].get_READY_HIT()
        if start == "1":
            break
    dut['regs'].RESET_HIT = 0
    fcsv = open(fname)
    hits = np.loadtxt(fcsv,
                      delimiter=",",
                      dtype=[("bxid", 'i4'), ("col", 'i2'), ("row", 'i2'), ("tot", 'i4'), ("trg", 'i'), ("comm", "S64")])
    if len(hits) > 0 and hits[-1]['col'] == -1:
        hits = hits[:-1]
    fcsv.close()
    return hits


def is_doneHIT(dut):
    dut['regs'].RESET_HIT = 1
    start = dut['regs'].get_READY_HIT()
    if start == "1":
        return True
    else:
        return False


def util_pauseHIT(dut):
    dut['regs'].CLK_HIT_GATE = 0


def util_resumeHIT(test_device):
    dut['regs'].CLK_HIT_GATE = 1
