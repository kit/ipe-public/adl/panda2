# Panda2-DAQ
[![Build Status]]()

DAQ for the [Panda2](https://adl.ipe.kit.edu/english) chip based on the [Basil](https://github.com/SiLab-Bonn/basil) framework.

## Installation

- Install [miniconda](https://conda.io/miniconda.html) for python

- Install dependencies and panda2-daq:
```
conda install numpy bitarray pyyaml scipy numba pytables matplotlib tqdm pyzmq blosc psutil
git clone https://github.com/SiLab-Bonn/basil.git
cd basil
python setup.py develop
# pip install git+https://git.scc.kit.edu/vt1680/panda2-daq.git@master
git clone https://git.scc.kit.edu/vt1680/panda2-daq.git@master
cd panda2-daq
python setup.py develop
```

- Install tools (Optional)
```
conda install jupyter notebook
conda install sphinx
```

- Install vivado

Download from [Xilinx](https://www.xilinx.com/support/download.html) and install vivado.

- tested version
   - 2017.2 for Nexy-video
   - 2023.2 for Genesys

## How to make document

```
cd docs
make singlehtml
firefox _build/singlehtml/index.html
```

## Start DAQ system

- Complie bit file and load the bit file
```
cd firmware/vivado
./make.sh
./program.sh
```

- Connect all (see docs)

- set IP address of PC (IP address of the FPGA is 192.168.10.16)

- Power the chip


## Examples

see examples

