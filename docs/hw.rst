###########################
Chip card
###########################

************************
Pad Description
************************

The pads are at bottom of the chip.
Table presents description of pads. Pin1 (VSSA) is the right most pad and Pin48 (subpix) is the left most pad.

.. csv-table:: Table: Pad List
    :file: _static/pinlist.csv
    :header-rows: 1


Wire bonding plan:

 .. image:: _static/wirebonding.png
       :width: 400

Schematic and layout:

    xxx xxxx.pdf
    
****************************************
Power supplies and reference voltages
****************************************

PANDA requires several external inputs

.. csv-table::
    :file: _static/connections.csv
    :header-rows: 1

VSS:

    VSS should be connected to a power source (TOELLNER only) as follows:.

  .. image:: _static/vss.png

Trigger out (optional):

    This signal can be connected to the external trigger input of a pulse generator. 
    It is Pin7 of Connector-JB on Nexy.

 .. image:: _static/jb.png
       :width: 400






