import os, time, sys
import tables as tb
import numpy as np
import yaml
import matplotlib.pyplot as plt

import panda2.analysis.interpreter as interpreter
import panda2.analysis.event_builder as event_builder
import panda2.analysis.fitfunctions as fitfunctions
from panda2.panda2 import NCOLS, NROWS, NCOMPS
import logging
logger = logging.getLogger()


def get_filename(fname, datdir=None):
    if datdir is not None:
        fname = os.path.join(datdir, fname)
    if 'hit.h5' == fname[-6:]:
        fhit = fname
        if fname[:-6].split('_')[-1] == 'th':
            fraw = fname[:-6] + 'search.h5'
        elif fname[:-6].split('_')[-1] == 'tdac':
            fraw = fname[:-6] + 'tune.h5'
        else:
            fraw = fname[:-6] + 'scan.h5'
    elif 'search.h5' == fname[-9:]:
        fraw = fname
        fhit = fname[:-9] + 'hit.h5'
    elif 'tune.h5' == fname[-7:]:
        fraw = fname
        fhit = fname[:-9] + 'hit.h5'
    else:
        fraw = fname
        fhit = fname[:-7] + 'hit.h5'
    return fraw, fhit


def analyze(fraw, fhit):
    # interpret and event_build
    interpreter.interpret_h5(fraw, fhit)
    logger.info('interpreted {}'.format(fhit))
    event_builder.build_h5(fraw=fraw, fhit=fhit)
    logger.info('alingned to timestamp {}'.format(fhit))


def load_scan_param(fraw, conf_name=[], karg_name=[]):
    additional_param = {}
    with tb.open_file(fraw) as f:
        param = f.root.scan_parameters[:]
        firmware = yaml.unsafe_load(f.root.meta_data.attrs.firmware_after)
        for k in karg_name:
            additional_param[k] = f.root.meta_data.get_attr(k)
        ram = f.root.ram_data[:]

        for k in conf_name:
            k_tmp = k.split('.')
            if k_tmp[0] == 'CONF':
                additional_param[k] = firmware[k_tmp[0]][k_tmp[1]]
            if k_tmp[0] == 'PixelConf':
                additional_param[k] = ram[ram['name']==k_tmp[1]+'before']['value']
            else:
                additional_param[k] = firmware[k_tmp[0]][k_tmp[1]]
    return param, additional_param


def load_all_data(fhit):
    with tb.open_file(fhit) as f:
        dat = f.root.event_data[:]
    return dat


def get_scurves(dat, param, clean_data=True, inj_n=100,
                outdir=None, fitting=False, return_data=True, plotting=False):
    if outdir is not None:
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        print(outdir)

    res_dtype = [('col', 'i'), ('row', 'i'), ('comp', 'i'), ('quick_th', 'f')]
    if fitting:
        res_dtype.append(('fit', 'f', 6))
    if len(param.dtype.names) > 3:
        names = [*param.dtype.names[3:]]
        unipara = np.unique(param[names])
        for n in names:
            res_dtype.append((n, param.dtype[n], param.dtype[n].shape))
    else:
        names = []
        unipara = [0]  # dummy
    # print(names, unipara)
    res = np.empty(len(unipara) * NROWS * NCOLS, dtype=res_dtype)  # 2 for just in case

    if return_data:
        res_data = []
    i = 0
    for up in unipara:
        if len(names) > 0:
            ev = param['scan_param_id'][param[names] == up]
            dat1 = dat[np.isin(dat['event_number'], ev)]
        else:
            ev = param['scan_param_id']
            dat1 = dat
        unipix = np.unique(dat1[['col', 'row', 'comp']])

        for upix in unipix:
            if 'cols' in up.dtype.names:
                if len(up.dtype['cols'].shape)==0:
                    cols = [up['cols']]
                else:
                    cols = up['cols']
                if upix['col'] not in cols:
                    print(up, upix, 'trash data')
                    continue
            if 'rows' in up.dtype.names:
                if len(up.dtype['rows'].shape)==0:
                    rows=[up['rows']]
                else:
                    rows=up['rows']
                if upix['row'] not in rows:
                    print(up, upix, 'trash data')
                    continue
            res[i]['col'] = upix['col']
            res[i]['row'] = upix['row']
            res[i]['comp'] = upix['comp']
            for n in names:
                res[i][n] = up[n]

            dat0 = dat1[dat1[['col', 'row', 'comp']] == upix]
            if clean_data:
                dat0 = dat0[(dat0['err']==0)&(dat0['comp']<2)]
                uni, idx = np.unique(dat0['timestamp'], return_index=True)
                dat0 = dat0[idx]

            uni, nhit = np.unique(dat0['event_number'], return_counts=True)
            x = param[ev]['inj']
            arg = np.argsort(x)
            y = np.zeros(len(x), dtype=nhit.dtype)
            y[np.isin(ev, uni)] = nhit
            x = x[arg]
            y = y[arg]
            if return_data:
                res_data.append([param['inj'][uni], nhit])
            # logger.info('pix={} {} {}'.format(upix, up, len(nhit)))
            if fitting:
                try:
                    res[i]['fit'] = fitfunctions.fit_scurve(x, y, A=inj_n, reverse=False)
                except:
                    print("fitting error", upix, up)
                    res[i]['fit'] = [np.nan] * 6
            if len(y) < 2:
                res[i]['quick_th'] = np.nan
            else:
                res[i]['quick_th'] = fitfunctions.find_value(x, fitfunctions.find_arg(y, 50))
            if plotting:
                fig, ax = plt.subplots()
                ax.plot(x, y, 'o')
                ax.set_title('{0}-{1} th={2: .4f}'.format(
                    upix['col'], upix['comp'], res[i]['quick_th']))
                if fitting:
                    ax.plot(np.linspace(x[0], x[-1], 100),
                            fitfunctions.scurve(
                                np.linspace(x[0], x[-1], 100),
                                *res[i]['fit'][:3]),
                                '-',
                                label='th={0:.3f}, sigma={1:.3f}'.format(res[i]['fit'][1], res[i]['fit'][2]),
                                )
                    ax.legend()
                ax.set_ylim(0, inj_n * 1.05)
                ax.set_ylabel('Inj[V]')
                ax.set_ylabel('#')
                plt.savefig(os.path.join(outdir, '{}-{}-{}_{}={}.png'.format(upix['col'], upix['row'], upix['comp'], names, up)))
                plt.close(fig)
            i = i + 1
            # break
        # break
    res = res[:i]
    np.save(os.path.join(outdir,'scurve.npy'),
            {'res':res, 'res_data':res_data})
    if return_data:
        return res, res_data
    return res


def get_ts(dat, param, extra_param, clean_data=False, plotting=False, outdir=None):
    res_dtype = [('event_number', 'u8'), ('col', 'i'), ('comp', 'i'),
                 ('toa', 'f'), ('tot', 'f'), ('tdc', 'f'),
                 ('toa_std', 'f'), ('tot_std', 'f'), ('tdc_std', 'f')]
    dat = dat[np.argsort(dat[['col', 'comp', 'event_number']])]
    uni, idx = np.unique(dat[['col', 'comp', 'event_number']], return_index=True)
    dat_split = np.split(dat, idx[1:])
    res_ts = np.empty(len(uni), dtype=res_dtype)
    res_ts['col'] = uni['col']
    res_ts['comp'] = uni['comp']
    res_ts['event_number'] = uni['event_number']

    for d_i, d in enumerate(dat_split):
        res_ts[d_i]['toa'] = np.average(d['toa'])
        res_ts[d_i]['toa_std'] = np.std(d['toa'])
        res_ts[d_i]['tot'] = np.average(d['tot'])
        res_ts[d_i]['tot_std'] = np.std(d['tot'])
        res_ts[d_i]['tdc'] = np.average(d['tdc'])
        res_ts[d_i]['tdc_std'] = np.std(d['tdc'])

    if plotting:
        if outdir is None:
            pass
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        if extra_param['plsgen_phase'] == 'fpga':
            period = 32 * 4
        else:
            period = 10E-9
        uni_pix, idx_pix, cnt_pix = np.unique(
            res_ts[['col', 'comp']],
            return_index=True,
            return_counts=True)
        for u_i, u in enumerate(uni_pix):
            tmp = res_ts[idx_pix[u_i]:idx_pix[u_i] + cnt_pix[u_i]]
            phase = param[tmp['event_number']]['phase']

            fig, ax = plt.subplots(nrows=5, sharex=False, figsize=[5, 12])
            ax[0].errorbar(phase, tmp['toa'], yerr=tmp['toa_std'], fmt='C0.')
            ax[1].errorbar(phase, tmp['tot'], yerr=tmp['tot_std'], fmt='C0.')
            ax[2].errorbar(phase, tmp['tdc'], yerr=tmp['tdc_std'], fmt='C0.')

            i = 0
            while True:
                arg = np.argwhere((phase >= period * i) & (phase < period * (i + 1)))
                if len(arg) == 0:
                    break
                ax[3].errorbar(phase[arg] - period * i, tmp['tdc'][arg], fmt='.-')
                ax[4].errorbar(phase[arg] - period * i, tmp['toa'][arg], fmt='.-')
                i = i + 1

            title = '{}-{}'.format(u['col'],u['comp'])
            ax[0].set_title(title)
            fig.tight_layout()
            fig.savefig(os.path.join(outdir, title+'.png'))
            ax[2].set_xlabel('Phase[ns]')
            ax[0].set_ylabel('Averaged ToA[1=5ns]')
            ax[1].set_ylabel('Averaged ToT[1=5ns]')
            ax[2].set_ylabel('Averaged TDC[a.u.]')

    return res_ts


def get_scurve_tdac(dat, param, clean_data=True, inj_n=100,
                outdir=None, fitting=False, return_data=True, plotting=False):
    if outdir is not None:
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        print(outdir)

    res_dtype = [('col', 'i'), ('row', 'i'), ('comp', 'i'), ('tdac', 'i')]
    if len(param.dtype.names) > 4:
        names = [*param.dtype.names[2:-2]]
        unipara = np.unique(param[names])
        for n in names:
            res_dtype.append((n, param.dtype[n], param.dtype[n].shape))
    else:
        names = []
        unipara = [0]  # dummy
    # print(names, unipara)
    res = np.empty(len(unipara) * NROWS * NCOLS, dtype=res_dtype) # NROWS * NCOLS for just in case

    if return_data:
        res_data = []
    i = 0
    for up in unipara:
        if len(names) > 0:
            ev = param['scan_param_id'][param[names] == up]
            dat1 = dat[np.isin(dat['event_number'], ev)]
        else:
            ev = param['scan_param_id']
            dat1 = dat
        unipix = np.unique(dat1[['col', 'row', 'comp']])

        for upix in unipix:
            if 'cols' in up.dtype.names:
                if len(up.dtype['cols'].shape)==0:
                    cols = [up['cols']]
                else:
                    cols = up['cols']
                if upix['col'] not in cols:
                    print(up, upix, 'trash data')
                    continue
            if 'rows' in up.dtype.names:
                if len(up.dtype['rows'].shape)==0:
                    rows = [up['rows']]
                else:
                    rows = up['rows']
                if upix['row'] not in rows:
                    print(up, upix, 'trash data')
                    continue
            res[i]['col'] = upix['col']
            res[i]['row'] = upix['row']
            res[i]['comp'] = upix['comp']
            for n in names:
                res[i][n] = up[n]

            dat0 = dat1[dat1[['col', 'row', 'comp']] == upix]
            if clean_data:
                dat0 = dat0[(dat0['err'] == 0) & (dat0['comp'] < 2)]
                uni, idx = np.unique(dat0['timestamp'], return_index=True)
                dat0 = dat0[idx]

            uni, nhit = np.unique(dat0['event_number'], return_counts=True)
            x = param[ev]['tdacs'][0] + param[ev]['comps'][0] * 8   ## TODO
            arg = np.argsort(x)
            y = np.zeros(len(x), dtype=nhit.dtype)
            y[np.isin(ev, uni)] = nhit
            x = x[arg]
            y = y[arg]
            if return_data:
                res_data.append([param['inj'][uni], nhit])
            # logger.info('pix={} {} {}'.format(upix, up, len(nhit)))
            if fitting:
                try:
                    res[i]['fit'] = fitfunctions.fit_scurve(x, y, A=inj_n, reverse=False)
                except:
                    print("fitting error", upix, up)
                    res[i]['fit'] = [np.nan] * 6
            if len(y) < 2:
                res[i]['quick_th'] = np.nan
            else:
                res[i]['quick_th'] = fitfunctions.find_value(x, fitfunctions.find_arg(y, 50))
            if plotting:
                fig, ax = plt.subplots()
                ax.plot(x, y, 'o')
                ax.set_title('{0}-{1} th={2: .4f}'.format(
                    upix['col'], upix['comp'], res[i]['quick_th']))
                if fitting:
                    ax.plot(
                        np.linspace(x[0], x[-1], 100),
                        fitfunctions.scurve(np.linspace(x[0], x[-1], 100),*res[i]['fit'][:3]),
                        '-',
                        label='th={0:.3f}, sigma={1:.3f}'.format(res[i]['fit'][1], res[i]['fit'][2]),
                        )
                    ax.legend()
                ax.set_ylim(0, inj_n * 1.05)
                ax.set_ylabel('Inj[V]')
                ax.set_ylabel('#')
                plt.savefig(os.path.join(outdir, '{}-{}-{}_{}={}.png'.format(upix['col'], upix['row'], upix['comp'], names, up)))
                plt.close(fig)
            i = i + 1
            # break
        # break
    res = res[:i]
    np.save(os.path.join(outdir, 'scurve.npy'),
            {'res': res, 'res_data': res_data})
    if return_data:
        return res, res_data
    return res