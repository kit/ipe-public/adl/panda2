############
Introduction
############

Panda2 is a prototype chip for ......

PANDA2 chip summary
===========================

CMOS Process:

  - TSI 1xx nm process (run2021)

Wafer:

  - Standard wafer 
  - High registive wafer 370 ohm/cm2

    + 700 um (w/o thinnning)
    + 100 um
    + 50 um

  - High registive wafer 6k ohm/cm2
    + 700 um (w/o thinnning)
    + 300 um

Sizes:

  - Chip size:5 mm x 5 mm
  - Pixel size: 165 um x 50 um
  - Number of pixels: 29 columns x 62 rows

Readout electronics:

  - n-well PAD separated from VDDA
  - 2 comparator / pixel (124 hit buffers / column)

    + Faster comparator (good for ToA) in odd rows
    + Slower comparator (good for ToT) in even rows

  - 2-phase shift registers for pixel, global DAC configuration
  - triggerless readout with 40MHz timestamp
  - hit data with bit Time-of-Arrival and bits Time-over-Threshod in 40 MHz timestamp
  - 8b10b data stream


Revisions
========================

v1.0: 2023-02-14

  - Created

