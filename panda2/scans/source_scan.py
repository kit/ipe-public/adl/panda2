import time

import panda2.scan_base as scan_base
import panda2.scan_base as scan_base
import panda2.analysis.interpreter as interpreter
import panda2.analysis.event_builder as event_builder


local_configuration={"with_trig": False,
                     "with_rx1": False,
                     "with_mon": False,
                     "scan_time": 10, ## in second
}


class SourceScan(scan_base.ScanBase):
    scan_id = "source_scan"
      
    def scan(self, **kwargs): 
        scan_time = kwargs.pop('scan_time', 10)
        with_trig = kwargs.pop('with_trig', local_configuration['with_trig'])
        cnt = 0
        scanned = 0
        with self.readout(scan_param_id=0,
                          fill_buffer=False,
                          clear_buffer=True,
                          reset_rx=False,
                          reset_sram_fifo=False,
                          readout_interval=0.2,
                          timeout=0):

            self.dut.mask_rx(False)
            t0 = time.time()
            while True:
                pre_cnt = cnt
                cnt = self.fifo_readout.get_record_count()
                pre_scanned = scanned
                scanned = time.time() - t0
                self.logger.info('time=%.0fs dat=%d rate=%.3fk/s'%(scanned, cnt, (cnt - pre_cnt)/(scanned - pre_scanned)/1024))
                if scanned + 10 > scan_time and scan_time > 0:
                    break
                elif scanned < 30:
                    time.sleep(1)
                else:
                    time.sleep(10)
            time.sleep(max(0, scan_time - scanned))
            self.dut.mask_rx(True)

    def analyze(self, fraw=None):
        if fraw is None:
            fraw = self.output_filename + '.h5'
        fhit = fraw[:-7] + 'hit.h5'

        # interpret and event_build
        interpreter.interpret_h5(fraw, fhit)
        self.logger.info('interpreted {}'.format(fhit))
        #event_builder.build_h5(fraw=fraw, fhit=fhit)
        self.logger.info('alingned to timestamp {}'.format(fhit))
        return fhit

    def plot(self, fraw=None):
        pass

    
if __name__ == "__main__":
    import panda2.panda2 as panda2
    import argparse
    
    parser = argparse.ArgumentParser(usage="python source_scan.py",
             formatter_class = argparse.RawTextHelpFormatter)
    parser.add_argument("--config_file", type=str, default=None)
    parser.add_argument("-time", '--scan_time', type=int, default=None,
                        help="Scan time in seconds.")
    args = parser.parse_args()
    
    dut = panda2.Panda2()
    dut.init()
    if args.config_file is None:
        dut.set_sync()
        dut.set_rx()
    else:
        dut.load_config(args.config_file)
    if args.scan_time is not None:
        local_configuration["scan_time"]=args.scan_time

    scan = SourceScan(dut, online_monitor_addr="tcp://127.0.0.1:5500")
    scan.start(**local_configuration)
    scan.analyze()