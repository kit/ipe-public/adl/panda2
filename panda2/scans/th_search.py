import time

import numpy as np
import tables as tb

import panda2.scan_base as scan_base
import panda2.analysis.interpreter as interpreter
import panda2.analysis.event_builder as event_builder
from panda2.panda2 import NCOLS, NROWS, NCOMPS
import panda2.analysis.data_loader as data_loader

local_configuration = {
    "inj": [0.015, 0.025, 1.0],  # np.arange(0.1,0.6,0.05),
    'phase': None,
    "cols": np.arange(NCOLS),
    "rows": np.arange(NROWS),
    "comps": None,  # np.arange(NCOMPS),
    "n_inj_col": 1,
    'n_inj_row': 1,
    'n_inj_comp': 1,
    "plsgen": None,
    "disable_noninjected_pixel": True,
    # parameter for auto-step
    'wait_neg_amp': 0.1,
    'hitth_coarse': 0.1,  # 0-1 ratio of recieved hit
    'n_finalize': 5,      # n of "over-run" inj steps
    'hitth_fine': 0.97,   # 0-1 ratio of recieved hit per pix
    'run_mode': 'auto',   # auto, dry_run, fixed
}


class ThSearch(scan_base.ScanBase):
    scan_id = "th_search"

    def scan(self, **kwargs):
        """ List of kwargs
            cols: list of pixel
            rows: list of pixel
            n_mask_pix: number of pixels injected at onece
            injlist: list of inj (inj_high-inj_low)
            with_mon: get timestamp of mon (mon will be enabled)
        """
        ####################
        # get options from args
        plsgen = kwargs.pop("plsgen", None)
        if plsgen is None:
            plsgen = self.dut
            inj_type = 'inj'
        elif isinstance(plsgen, str):
            if plsgen == 'trig':
                inj_type = 'trig'
            else:
                inj_type = 'inj'
            plsgen = self.dut
        else:
            inj_type = 'external'

        disable_noninjected_pixel = kwargs.pop("disable_noninjected_pixel", local_configuration["disable_noninjected_pixel"])
        en_comp_org = np.append(
            np.expand_dims(np.copy(self.dut.PixelConf['en_comp0']), axis=2),
            np.expand_dims(np.copy(self.dut.PixelConf['en_comp1']), axis=2),
            axis=2)
        en_comp = np.copy(en_comp_org)
        # ### param for th_search
        self.wait_neg_amp = kwargs.pop("wait_neg_amp", local_configuration['wait_neg_amp'])
        self.n_finalize = kwargs.pop("n_finalize", local_configuration['n_finalize'])
        self.hitth_coarse = kwargs.pop("th_coarse", local_configuration['hitth_coarse'])
        self.hitth_fine = kwargs.pop("th_coarse", local_configuration['hitth_fine'])
        self.injlist = kwargs.pop("inj", local_configuration['inj'])
        # ### debug
        run_mode = kwargs.pop("run_mode", local_configuration['run_mode'])

        ####################
        # get scan params from args
        param_dtype = [("scan_param_id", "<i4"), ("timestamp", '<f8'), ('inj', '<f4')]
        scan_param = {}
        nsteps = 1
        phaselist = kwargs.pop("phase", local_configuration['phase'])
        if phaselist is not None:
            if inj_type == 'external':
                param_dtype.append(('phase', 'f4'))
            else:
                param_dtype.append(('phase', 'u4'))
            scan_param['phase'] = phaselist
            nsteps = nsteps * len(scan_param['phase'])

        tdaclist = kwargs.pop('tdac', None)
        if tdaclist is not None:
            if isinstance(tdaclist, int):
                tdaclist = np.array([tdaclist])
            elif isinstance(tdaclist, list):
                tdaclist = np.array(tdaclist)
            param_dtype.append(('tdac', '<u1'))
            scan_param['tdac'] = tdaclist
            nsteps = nsteps * len(scan_param['tdac'])
            tdac_value = tdaclist[0]
        tdac_org = np.append(
            np.expand_dims(np.copy(self.dut.PixelConf['tdac0']), axis=2),
            np.expand_dims(np.copy(self.dut.PixelConf['tdac1']), axis=2),
            axis=2)
        tdac = np.copy(tdac_org)

        rows = kwargs.pop("rows", local_configuration['rows'])
        if rows is None:
            rows = np.unique(np.argwhere(en_comp_org[:, :, :] == 1)[:, 1])
        else:
            if isinstance(rows, int):
                rows = np.array([rows], dtype=np.int8)
            n_inj_row = min(kwargs.pop("n_inj_row", local_configuration['n_inj_row']), len(rows))
            n_loop_row = (len(rows) - 1) // n_inj_row + 1
            rowlist = np.ones([n_loop_row, n_inj_row], dtype='<u2') * 0xFFFF
            for i, r in enumerate(rows):
                div, mod = divmod(i, n_loop_row)
                rowlist[mod, div] = r
            param_dtype.append(("rows", "<u2", (n_inj_row)))
            scan_param['rows'] = rowlist
            nsteps = nsteps * n_loop_row
            rows = rowlist[0, :]

        cols = kwargs.pop("cols", local_configuration['cols'])
        if cols is None:
            cols = np.unique(np.argwhere(en_comp_org[:, :, :] == 1)[:, 0])
        else:
            if isinstance(cols, int):
                cols = np.array([cols], dtype=np.int8)
            n_inj_col = min(kwargs.pop("n_inj_col", local_configuration['n_inj_col']), len(cols))
            n_loop_col = (len(cols) - 1) // n_inj_col + 1
            collist = np.ones([n_loop_col, n_inj_col], dtype='<u1') * 0xFF
            for i, c in enumerate(cols):
                div, mod = divmod(i, n_loop_col)
                collist[mod, div] = c
            param_dtype.append(("cols", "<u1", (n_inj_col)))
            scan_param['cols'] = collist
            nsteps = nsteps * n_loop_col
            cols = collist[0, :]

        comps = kwargs.pop("comps", local_configuration['comps'])
        if comps is None:
            comps = np.unique(np.argwhere(en_comp_org[:, :, :] == 1)[:, 2])
        else:
            if isinstance(comps, int):
                comps = np.array([comps], dtype=np.int8)
            n_inj_comp = min(kwargs.pop("n_inj_comp", local_configuration['n_inj_comp']), len(comps))
            n_loop_comp = (len(comps) - 1) // n_inj_comp + 1
            complist = np.ones([n_loop_comp, n_inj_comp], dtype='<u2') * 0xFFFF
            for i, r in enumerate(comps):
                div, mod = divmod(i, n_loop_comp)
                complist[mod, div] = r
            param_dtype.append(("comps", "<u2", (n_inj_comp)))
            scan_param['comps'] = complist
            nsteps = nsteps * n_loop_comp
            comps = complist[0, :]

        for k in kwargs:
            param_dtype.append((k, '<u2'))
            scan_param[k] = kwargs[k]
            print(k, kwargs[k])
            nsteps = nsteps * len(scan_param[k])

        # ###################
        # create a table for scan_params
        param_dtype = np.dtype(param_dtype)
        self.scan_param_table = self.h5_file.create_table(
            self.h5_file.root,
            name='scan_parameters',
            title='scan_parameters',
            description=param_dtype,
            filters=tb.Filters(complib='zlib', complevel=5, fletcher32=False))
        for k, v in scan_param.items():
            self.meta_data_table.attrs[k] = v
        current_param = {}
        # ###################
        # enable readout
        self.dut.mask_rx(True)
        if inj_type == 'trig':
            ts_n = self.dut['trig']['REPEAT']
        else:
            ts_n = self.dut['inj']['REPEAT']
        with self.readout(scan_param_id=-1,                       # TODO open/close self.readout for each scan step (make it faster)
                          fill_buffer=True, clear_buffer=True,
                          reset_rx=False, reset_sram_fifo=False,  # set_rx, does these resets
                          readout_interval=0.001):
            dqdata = self.fifo_readout.data
            # ###################
            # main scan loop
            self.logger.info('starting scan nsteps={}'.format(nsteps))

            for param_id in range(nsteps):
                # update scan parameters
                scan_div = 1
                for pname, plist in scan_param.items():
                    plen = len(plist)
                    mod = (param_id // scan_div) % plen
                    self.scan_param_table.row[pname] = plist[mod]
                    current_param[pname] = plist[mod]
                    if param_id // scan_div != (param_id - 1) // scan_div:
                        self.logger.info('Thscan: '.join(['pname=', str(pname), '=', str(plist[mod]), ' step=', str(param_id),
                                         ' scan_div=', str(scan_div), ' plen=', str(plen), ' mod=', str(mod)]))
                        if pname == 'phase':
                            plsgen.set_inj_phase(plist[mod])
                        elif pname == 'cols':
                            cols = plist[mod][plist[mod] != 0xFF]
                            self.dut.set_en_inj(cols=cols)
                        elif pname == 'rows':
                            rows = plist[mod][plist[mod] != 0xFFFF]
                            self.dut.set_en_inj(rows=rows)
                        elif pname == 'comps':
                            comps = plist[mod][plist[mod] != 0xFFFF]
                        elif pname == 'tdac':
                            tdac_value = plist[mod]
                        else:
                            self.dut.set_conf(**{pname: plist[mod]})
                    scan_div = scan_div * plen

                if 'tdac' in scan_param:
                    tdac[:, :, :] = tdac_org[:, :, :]
                    tdac[cols, rows, comps] = tdac_value

                if disable_noninjected_pixel:
                    en_comp[:, :, :] = 0
                    # print(cols, rows, comps, en_comp.shape)
                    en_comp[cols, rows, comps] = 1
                self.dut.set_ram(en_comp, tdac)
                npix = (en_comp == 1).sum()  # TODO en_inj and en_comp is not the same
                ####################
                # search s-curve

                amp, status = self.init_step(npix=npix,
                                             run_mode=run_mode)
                plsgen.set_inj_amp(amp, unit='V')
                while status != 'done':
                    self.scan_param_id = self.scan_param_id + 1
                    self.dut.mask_rx(False)
                    if inj_type == 'trig':
                        self.dut.inject_trig()
                    else:
                        self.dut.inject()

                    # count data
                    ts_len = 0
                    hit_len = 0
                    while True:
                        time.sleep(0.01)
                        try:
                            data = dqdata.popleft()[0]
                            ts_len = ts_len + (data & 0xE000_0000 == 0x8000_0000).sum()
                            hit_len = hit_len + (data & 0xF000_0000 == 0x1000_0000).sum()
                            if hit_len > npix * ts_n * 5 * 10:  # 10 is just a factor
                                break
                            # self.logger.info('ts={} hit={} {}'.format(ts_len, hit_len,ts_n))
                        except IndexError:
                            if ts_len >= ts_n * 3:
                                break

                    self.scan_param_table.row['scan_param_id'] = self.scan_param_id
                    self.scan_param_table.row['inj'] = amp
                    self.scan_param_table.row['timestamp'] = time.time()
                    self.scan_param_table.row.append()
                    self.scan_param_table.flush()

                    for k, v in current_param.items():
                        self.scan_param_table.row[k] = v

                    self.dut.mask_rx(True)
                    next_amp, status = self.next_step(hit_len, ts_len, amp)
                    plsgen.set_inj_amp(next_amp, unit='V')
                    log = 'ThScan: step={0} id={1} amp={2:.3f}'.format(param_id, self.scan_param_id, amp,)
                    self.logger.info(log + ' {0:.3f} st={1:s}{2} ts={3} hit={4}'.format(
                        next_amp, status, self.final_step, ts_len, hit_len))
                    amp = next_amp

        self.dut.set_ram(en_comp_org, tdac_org)

    def init_step(self, npix, run_mode='auto'):
        if run_mode == 'fixed':
            self.status = 'fine'
        elif run_mode == 'dry_run':
            self.status = 'done'
        else:
            self.status = 'coarse'

        self.final_step = self.n_finalize
        self.npix = npix
        amp = self.injlist[0]
        return amp, self.status

    def next_step(self, hit_len, ts_len, amp):
        if amp > self.injlist[-1]:
            self.status = 'done'
        elif hit_len >= 1 * 5 * self.hitth_coarse * (ts_len // 3) and self.status == 'coarse':
            self.status = 'fine'
            amp = amp - (self.injlist[1] - self.injlist[0]) * 10
        elif hit_len >= self.npix * 5 * self.hitth_fine * (ts_len // 3) and self.status == 'fine':
            self.status = 'finalize'
            self.final_step = self.n_finalize
        elif self.status == 'finalize':
            if self.final_step == 0:
                self.status = 'done'
            else:
                self.final_step = self.final_step - 1
        if self.status == 'coarse':
            amp = amp + (self.injlist[1] - self.injlist[0]) * 10
        elif self.status == 'fine':
            amp = amp + (self.injlist[1] - self.injlist[0])
        elif self.status == 'finalize':
            amp = amp + (self.injlist[1] - self.injlist[0])
        amp = np.clip(amp, self.injlist[0], self.injlist[-1] + self.injlist[1] - self.injlist[0])
        return amp, self.status

    def analyze(self):
        fraw = self.output_filename + '.h5'
        fhit = '_'.join(self.output_filename.split('_')[:-1]) + '_hit.h5'

        # interpret and event_build
        interpreter.interpret_h5(fraw, fhit)
        self.logger.info('interpreted {}'.format(fhit))
        event_builder.build_h5(fraw=fraw, fhit=fhit)
        self.logger.info('alingned to timestamp {}'.format(fhit))

        dat = data_loader.load_all_data(fhit)
        param, extra_param = data_loader.load_scan_param(fraw, conf_name=['PixelConf.en_comp', 'inj.REPEAT'])
        inj_n = extra_param['inj.REPEAT']
        ret, scurves = data_loader.get_scurves(dat,
                                               param,
                                               fitting=True,
                                               plotting=True,
                                               inj_n=inj_n,
                                               outdir='_'.join(fraw.split('_')[:-2]),
                                               return_data=True)

        # with tb.open_file(fhit, 'a') as f:
        #     f.create_table(
        #         f.root,
        #         name='get_scurves',
        #         description=ret.dtype,
        #         title='get_scurves',
        #         filters=self.filter_tables,
        #     )
        #     f.append(ret)
        #     f.flush()
        return fhit, ret, scurves

    def plot(self, ret, scurves):
        pass


if __name__ == "__main__":
    from panda2.panda2 import panda2
    dut = panda2()
    dut.init()
    dut.set_en_comp('all')

    dut.set_inj(inj_n=100, ext=True)
    dut.set_inj_amp(0.1, unit='V')

    scan = ThSearch(dut, online_monitor_addr="")
    scan.start(**local_configuration)
    scan.analyze()

