import time

import numpy as np

import panda2.scan_base as scan_base
import panda2.analysis.interpreter as interpreter
import panda2.analysis.event_builder as event_builder

local_configuration = {
    "cols": np.arange(29),
    "rows": np.arange(12),
    "comps": None,
    "n_inj_col": 1,
    'n_inj_row': 1,
    'n_inj_comp': 1,
    "plsgen": None,
    "disable_noninjected_pixel": True,
    # #### parameter for auto-step
    'hitth': 0.01,  # 0-1 ratio of recieved hit
    'run_mode': 'auto'
}


class TdacTune(scan_base.ScanBase):
    scan_id = "tdac_tune"

    def scan(self, **kwargs):
        """ List of kwargs
            cols: list of pixel
            rows: list of pixel
            n_mask_pix: number of pixels injected at onece
            injlist: list of inj (inj_high-inj_low)
            with_mon: get timestamp of mon (mon will be enabled)
        """
        ####################
        # get options from args
        plsgen = kwargs.pop("plsgen", None)
        if plsgen is None:
            plsgen = self.dut
            inj_type = 'inj'
            # self.inj_n = plsgen[inj_type].REPEAT
        elif isinstance(plsgen, str):
            if plsgen == 'trig':
                inj_type = 'trig'
            else:
                inj_type = 'inj'
            plsgen = self.dut
        else:
            inj_type = 'external'
            # self.inj_n = self.dut['inj'].REPEAT

        self.en_comp_org = np.append(
            np.expand_dims(np.copy(self.dut.PixelConf['en_comp0']), axis=2),
            np.expand_dims(np.copy(self.dut.PixelConf['en_comp1']), axis=2),
            axis=2)
        self.tdac_org = np.append(
            np.expand_dims(np.copy(self.dut.PixelConf['tdac0']), axis=2),
            np.expand_dims(np.copy(self.dut.PixelConf['tdac1']), axis=2),
            axis=2)

        disable_noninjected_pixel = kwargs.pop("disable_noninjected_pixel", local_configuration["disable_noninjected_pixel"])
        # ### param for autoscan
        self.hitth = kwargs.pop("hitth", local_configuration['hitth'])
        # self.noiseth = kwargs.pop("hitth", local_configuration['noiseth'])
        # ### debug
        # run_mode = kwargs.pop("run_mode", local_configuration['run_mode'])

        ####################
        # get scan params from args
        param_dtype = [("scan_param_id", "<i4"), ("timestamp", '<f8')]
        scan_param = {}
        nsteps = 1

        comps = kwargs.pop("comps", local_configuration['comps'])
        if comps is None:
            comps = np.unique((self.en_comp_org == 1)[:, 2])
            n_inj_comp = len(comps)
            n_loop_comp = 1
        else:
            if isinstance(comps, int):
                comps = np.array([comps], dtype=np.int8)
            n_inj_comp = min(kwargs.pop("n_inj_comp", local_configuration['n_inj_comp']), len(comps))
            n_loop_comp = (len(comps) - 1) // n_inj_comp + 1
            complist = np.ones([n_loop_comp, n_inj_comp], dtype='<u2') * 0xFFFF
            for i, r in enumerate(comps):
                div, mod = divmod(i, n_loop_comp)
                complist[mod, div] = r
            param_dtype.append(("comps", "<u2", (n_inj_comp)))
            scan_param['comps'] = complist
            nsteps = nsteps * n_loop_comp
            comps = complist[0, :]

        rows = kwargs.pop("rows", local_configuration['rows'])
        if rows is None:
            rows = np.unique((self.en_comp_org == 1)[:, 1])
            n_inj_row = len(rows)
            n_loop_row = 1
        else:
            if isinstance(rows, int):
                rows = np.array([rows], dtype=np.int8)
            n_inj_row = min(kwargs.pop("n_inj_row", local_configuration['n_inj_row']), len(rows))
            n_loop_row = (len(rows) - 1) // n_inj_row + 1
            rowlist = np.ones([n_loop_row, n_inj_row], dtype='<u2') * 0xFFFF
            for i, r in enumerate(rows):
                div, mod = divmod(i, n_loop_row)
                rowlist[mod, div] = r
            param_dtype.append(("rows", "<u2", (n_inj_row)))
            scan_param['rows'] = rowlist
            nsteps = nsteps * n_loop_row
            rows = rowlist[0, :]

        cols = kwargs.pop("cols", local_configuration['cols'])
        if cols is None:
            cols = np.unique((self.en_comp_org == 1)[:, 0])
            n_inj_col = len(cols)
            n_loop_col = 1
        else:
            if isinstance(cols, int):
                cols = np.array([cols], dtype=np.int8)
            n_inj_col = min(kwargs.pop("n_inj_col", local_configuration['n_inj_col']), len(cols))
            n_loop_col = (len(cols) - 1) // n_inj_col + 1
            collist = np.ones([n_loop_col, n_inj_col], dtype='<u1') * 0xFF
            for i, c in enumerate(cols):
                div, mod = divmod(i, n_inj_col)
                collist[div, mod] = c
            param_dtype.append(("cols", "<u1", (n_inj_col)))
            scan_param['cols'] = collist
            nsteps = nsteps * n_loop_col
            cols = collist[0, :]

        for k in kwargs:
            if k == 'inj':
                param_dtype.append((k, 'f4'))
            elif k == 'phase':
                param_dtype.append((k, 'u4'))
            else:
                param_dtype.append((k, '<u2'))
            scan_param[k] = kwargs[k]
            print(k, kwargs[k])
            nsteps = nsteps * len(scan_param[k])

        param_dtype.append(('tdacs', 'u1', n_inj_col * n_inj_comp * n_inj_row))
        param_dtype.append(('en_comps', 'u1', n_inj_col * n_inj_comp * n_inj_row))

        self.tdac = np.copy(self.tdac_org)
        self.en_comp = np.copy(self.en_comp_org)
        self.status = np.ones(self.tdac_org.shape, dtype='u1') * 0xFD  # -3 not searched, -2 lower than 0, -1 searching, 0x1 + tdac noise detected
        # ###################
        # create a table for scan_params
        param_dtype = np.dtype(param_dtype)
        self.scan_param_table = self.h5_file.create_table(
            self.h5_file.root,
            name='scan_parameters',
            title='scan_parameters',
            description=param_dtype,
            filters=self.filter_tables)
        for k, v in scan_param.items():
            self.meta_data_table.attrs[k] = v
        current_param = {}

        # ###################
        # enable readout
        self.dut.mask_rx(True)
        if inj_type == 'trig':
            ts_n = self.dut['trig']['REPEAT']
        else:
            ts_n = self.dut['inj']['REPEAT']
        with self.readout(scan_param_id=-1,                       # TODO open/close self.readout for each scan step (make it faster)
                          fill_buffer=True, clear_buffer=True,
                          reset_rx=False, reset_sram_fifo=False,  # set_rx, does these resets
                          readout_interval=0.001):
            dqdata = self.fifo_readout.data
            # ###################
            # main scan loop
            self.logger.info('starting scan nsteps={}'.format(nsteps))

            for param_id in range(nsteps):
                # update scan parameters
                scan_div = 1
                for pname, plist in scan_param.items():
                    plen = len(plist)
                    mod = (param_id // scan_div) % plen
                    self.scan_param_table.row[pname] = plist[mod]
                    current_param[pname] = plist[mod]
                    if param_id // scan_div != (param_id - 1) // scan_div:
                        # self.logger.info(''.join(['pname=', str(pname), '=', str(plist[mod]), ' step=', str(param_id),
                        #                          ' scan_div=', str(scan_div), ' plen=', str(plen), ' mod=', str(mod)]))
                        if pname == 'inj':
                            plsgen.set_inj_amp(plist[mod], unit='V')
                        elif pname == 'phase':
                            plsgen.set_inj_phase(plist[mod])
                        elif pname == 'cols':
                            # self.logger.info("cols {} {}".format(plist[mod], plist[mod][plist[mod] != 0xFF]))
                            cols = plist[mod][plist[mod] != 0xFF]
                            self.dut.set_en_inj(cols=cols)
                        elif pname == 'rows':
                            rows = plist[mod][plist[mod] != 0xFFFF]
                            self.dut.set_en_inj(rows=rows)
                        elif pname == 'comps':
                            comps = plist[mod][plist[mod] != 0xFFFF]
                        else:
                            self.dut.set_conf(**{pname: plist[mod]})
                    scan_div = scan_div * plen

                # ###################
                # search tdac
                next = self.init_step(cols, rows, comps, disable_noninjected_pixel)
                self.dut.set_ram(self.en_comp, self.tdac)
                npix = (self.en_comp == 1).sum()
                while next:
                    # set new scan_param_id and inject
                    self.scan_param_id = self.scan_param_id + 1
                    self.dut.mask_rx(False)
                    if inj_type == 'trig':
                        self.dut.inject_trig()
                    else:
                        self.dut.inject()

                    # count data
                    hit_len, ts_len = 0, 0
                    data = np.empty(0, dtype='uint32')
                    for _ in range(1000):
                        time.sleep(0.01)
                        try:
                            tmp = dqdata.popleft()[0]
                            # self.logger.info('{} {}'.format(len(tmp),tmp.dtype))
                            ts_len = ts_len + (tmp & 0xE000_0000 == 0x8000_0000).sum()
                            hit_len = hit_len + (tmp & 0xF000_0000 == 0x1000_0000).sum()
                            data = np.append(data, tmp)
                            if hit_len > npix * ts_n * 5 * 10:  # 10 is just a factor
                                break
                        except IndexError:
                            if ts_len >= ts_n * 3:
                                break

                    # update scan_param_table
                    self.scan_param_table.row['scan_param_id'] = self.scan_param_id
                    self.scan_param_table.row['timestamp'] = time.time()
                    param_tdacs = np.ones(n_inj_col * n_inj_comp * n_inj_row, dtype="u1") * 0xFF
                    param_en_comps = np.ones(n_inj_col * n_inj_comp * n_inj_row, dtype="u1") * 0xFF
                    i = 0
                    for c in cols:
                        for r in rows:
                            for co in comps:
                                param_tdacs[i] = self.tdac[c, r, co]
                                param_en_comps[i] = self.en_comp[c, r, co]
                                i = i + 1
                    self.scan_param_table.row['tdacs'] = param_tdacs
                    self.scan_param_table.row['en_comps'] = param_en_comps
                    self.scan_param_table.row.append()
                    self.scan_param_table.flush()
                    # prepare for next table data
                    for k, v in current_param.items():
                        self.scan_param_table.row[k] = v

                    self.dut.mask_rx(True)
                    next = self.next_status(data, cols, rows, comps)
                    self.logger.info('ThScan: step={0} id={1} st={2} ts={3} hit={4}'.format(param_id, self.scan_param_id, next, ts_len, hit_len))
                    self.dut.set_ram(self.en_comp, self.tdac)
                    npix = (self.en_comp == 1).sum()

        self.h5_file.root.ram_data.row['name'] = 'tdac0_tune'
        self.h5_file.root.ram_data.row['value'] = self.status[:, :, 0]
        self.h5_file.root.ram_data.row.append()
        self.h5_file.root.ram_data.row['name'] = 'tdac1_tune'
        self.h5_file.root.ram_data.row['value'] = self.status[:, :, 1]
        self.h5_file.root.ram_data.row.append()
        self.h5_file.root.ram_data.flush()

        self.dut.set_ram(self.en_comp_org, self.tdac_org)

    def init_step(self, cols, rows, comps, disable_noninjected_pixel):
        if disable_noninjected_pixel:
            self.en_comp[:, :, :] = 0
            self.en_comp[cols, rows, comps] = 1
        else:
            self.en_comp[:, :, :] = self.en_comp_org[:, :, :]
        self.tdac[:, :, :] = self.tdac_org[:, :, :]

        self.status[cols, rows, comps] = 0xFF
        self.status[((self.status == 0xFF) & (self.en_comp == 1))] = 0xFF
        return np.any(self.status == 0xFF)

    def next_status(self, data, cols, rows, comps):
        '''
        status 0xFF searching
               0xFE lower than 0
               0xFD not tuned
               8    noisy
               >=0  tuned TDAC value
        '''
        hit, ts, ts_trig = interpreter.raw2list(data)
        for c in cols:
            for r in rows:
                for co in comps:
                    cnt = ((hit['col'] == c) & (hit['row'] == r) & (hit['comp'] == co)).sum()
                    log = "pix{}-{}-{} cnt={} tdac={}".format(c, r, co, cnt, self.tdac[c, r, co])
                    if self.status[c, r, co] == 0xFF:                       # if the pixel is searching
                        if cnt >= (len(ts) + len(ts_trig)):                 # if nhit is larger than n_inj, hits should be from noise
                            self.tdac[c, r, co] = min(self.tdac[c, r, co] + 1, 7)  # go back one tdac
                            self.status[c, r, co] = 0x10 + self.tdac[c, r, co]
                            self.en_comp[c, r, co] = 0
                        elif cnt >= self.hitth * (len(ts) + len(ts_trig)):  # if nhit larger than threshold
                            self.status[c, r, co] = self.tdac[c, r, co]     # take this tdac value
                            self.en_comp[c, r, co] = 0
                        elif self.tdac[c, r, co] == 0:                      # if nhit=0  the mark as "lower than 0"
                            self.status[c, r, co] = 0xFE
                            self.en_comp[c, r, co] = 0
                        else:
                            self.tdac[c, r, co] = self.tdac[c, r, co] - 1
                    self.logger.info('{0} --> tdac={1} en_comp={2} st=0x{3:x}'.format(
                        log,
                        self.tdac[c, r, co],
                        self.en_comp[c, r, co],
                        self.status[c, r, co]))

        return np.any(self.status == 0xFF)

    def analyze(self):
        fraw = self.output_filename + '.h5'
        fhit = fraw[:-7] + 'hit.h5'

        # interpret and event_build
        interpreter.interpret_h5(fraw, fhit)
        self.logger.info('interpreted {}'.format(fhit))
        event_builder.build_h5(fraw=fraw, fhit=fhit)
        self.logger.info('alingned to timestamp {}'.format(fhit))
        return fhit

    def plot(self):
        pass
        # fraw = self.output_filename + '.h5'
        # fhit = self.output_filename[:-4] + 'hit.h5'
        # fpdf = self.output_filename + '.pdf'


if __name__ == "__main__":

    from panda2.panda2 import panda2
    dut = panda2()
    dut.init()
