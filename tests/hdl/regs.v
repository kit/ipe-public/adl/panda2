`timescale 1ns / 1ps
`default_nettype none

module regs #(
    parameter BASEADDR = 32'h0000,
    parameter HIGHADDR = 32'h0000,
    parameter ABUSWIDTH = 16
) (
    input wire BUS_RD,
    input wire BUS_WR,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    inout wire [7:0] BUS_DATA,
    input wire BUS_CLK,
    input wire BUS_RST,

    output wire RESET_HIT,
    output wire CLK_HIT_GATE,
    input  wire READY_HIT

);

reg [7:0] IP_DATA_OUT;
wire IP_RD,IP_WR;
wire [7:0] IP_DATA_IN;
wire [15:0] IP_ADD;

bus_to_ip #(
    .BASEADDR(BASEADDR), 
    .HIGHADDR(HIGHADDR), 
    .ABUSWIDTH(ABUSWIDTH) 
) i_bus_to_ip_top (
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),

    .IP_RD(IP_RD),
    .IP_WR(IP_WR),
    .IP_ADD(IP_ADD),
    .IP_DATA_IN(IP_DATA_IN),
    .IP_DATA_OUT(IP_DATA_OUT)

);
always @(posedge BUS_CLK) begin
    if(IP_RD) begin
        if(IP_ADD == 0)
          IP_DATA_OUT <= 8'b00000001;
        else if(IP_ADD ==1)
            IP_DATA_OUT <= {5'b000,RESET_HIT, CLK_HIT_GATE, READY_HIT};
        else if(IP_ADD ==2)
          IP_DATA_OUT <= tb.pcb.dac1[7:0];
        else if(IP_ADD ==3)
          IP_DATA_OUT <=  {2'b00, tb.pcb.dac1[13:8]}; 
        else
            IP_DATA_OUT <= 8'b0;
    end
end
reg [7:0] status_regs;
always @(posedge BUS_CLK) begin
    if (BUS_RST) 
        status_regs <= 0;
    if(IP_WR && IP_ADD ==1)
        status_regs <= IP_DATA_IN;
end
assign CLK_HIT_GATE = status_regs[1];
assign RESET_HIT = status_regs[2];

endmodule
