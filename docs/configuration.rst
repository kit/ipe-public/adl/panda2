########################
Configurations
########################

The configuration bits are accessed through a 2-phase shift register.
The block diagram of the 2-phase shift registers is:


The timing of the input and output signals are:

.. image:: _static/shiftreg.png

The order of the configuration bits is given below:

.. csv-table:: Configuration Bits
    :file: _static/config.csv
    :header-rows: 1

.. csv-table:: Pixel Configuation Bits
    :file: _static/pixconfig.csv
    :header-rows: 1

================================
Configurations for Analog part
================================

Each pixel contains a charge sensitive amplifierand comparator. Some of values can be optimized by DACs.

.. image:: _static/analog-1.png

DACs 
========================

BL:

  - DACVBL (global 8bits DAC) tunes the base line of the comparator input.
  - the voltage of the base line can be monitored by PAD "Baseline"

TDAC and en_comp:

  - To write RAM in the pixels, shift in TDAC/en_comp values with WrRAM of the target row =1, then shift in the values again with WrRAM=0.


Test pulse injection and Monitors
===================================

.. image:: _static/switches.png

Injection:

  - Each pixel in rows has a test pulse injection.
  - To enable the injection, set en_injection_col and en_injection_row to 1.
  - Use even hitbuffer-rows for this.
  - The injection enable-switch is AND of en_injection_col and en_injection_row.

Ampout:

  - Pixels in Row0 has ampout, output of the in-pixel charge sensitive amplifire (see Block Diagram of Amplifire).
  - To enable the ampout, set en_ampout_col to 1. Ampout is positive pulse.

Hitbus:

  - en_hitbus_col is ACTIVE LOW. to enable the hitbus, set 0.




