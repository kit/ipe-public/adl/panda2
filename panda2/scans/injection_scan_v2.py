import time

import numpy as np

import panda2.scan_base as scan_base
import panda2.analysis.interpreter as interpreter
import panda2.analysis.event_builder as event_builder

local_configuration = {
    "inj": [0.8],  # np.arange(0.1,0.6,0.05),
    'phase': None,
    "cols": np.arange(29),
    "rows": np.arange(12),
    "comps":None,
    "n_inj_col": 1,
    'n_inj_row': 1,
    'dry_run': False,
    "plsgen": None,
    'plsgen_phase': 'fpga',
    "disable_noninjected_pixel": False,
}


class InjectionScan(scan_base.ScanBase):
    scan_id = "injection_scan"

    def scan(self, **kwargs):
        """ List of kwargs
            cols: list of pixel
            rows: list of pixel
            n_mask_pix: number of pixels injected at onece
            injlist: list of inj (inj_high-inj_low)
            with_mon: get timestamp of mon (mon will be enabled)
        """
        ####################
        # get options from args
        plsgen = kwargs.pop("plsgen", None)
        if plsgen is None:
            plsgen = self.dut
            is_plgen_external=False
        else:
            is_plgen_external=True

        disable_noninjected_pixel = kwargs.pop("disable_noninjected_pixel", local_configuration["disable_noninjected_pixel"])
        comps = kwargs.pop('comps', local_configuration["comps"])
        if comps is None:
            comps = np.unique(np.argwhere(en_comp_org[:,:,:]!=0)[:,2])
        en_comp_org = np.append(np.expand_dims(np.copy(self.dut.PixelConf['en_comp0']) , axis=2), 
            np.expand_dims(np.copy(self.dut.PixelConf['en_comp1']) , axis=2),
            axis=2)
        en_comp = np.copy(en_comp_org)

        #### debug
        dry_run = kwargs.pop("dry_run", local_configuration['dry_run'])
        ####################
        # get scan params from args
        param_dtype = [("scan_param_id", "<i4"), ("timestamp", '<f8')]
        scan_param = {}
        nsteps = 1
        phaselist = kwargs.pop("phase", local_configuration['phase'])
        plsgen_phase = kwargs.pop("plsgen_phase", local_configuration['plsgen_phase'])
        if phaselist is not None:
            if plsgen_phase == 'external':
                param_dtype.append(('phase', 'f4'))
            else:
                param_dtype.append(('phase', 'u4'))
            scan_param['phase'] = phaselist
            nsteps = nsteps * len(scan_param['phase'])

            scan_param['phase'] = phaselist
            nsteps = nsteps * len(scan_param['phase'])

        injlist = kwargs.pop("inj", local_configuration['inj'])
        if injlist is not None:
            if isinstance(injlist, float):
                injlist = np.array([injlist])
            elif isinstance(injlist, list):
                injlist = np.array(injlist)
            param_dtype.append(('inj', '<f4'))
            scan_param['inj'] = injlist
            nsteps = len(scan_param['inj'])

        tdaclist = kwargs.pop('tdac', None)
        if tdaclist is not None:
            if isinstance(tdaclist, int):
                tdaclist = np.array([tdaclist])
            elif isinstance(tdaclist, list):
                tdaclist = np.array(tdaclist)
            param_dtype.append(('tdac', '<u1'))
            scan_param['tdac'] = tdaclist
            nsteps = nsteps * len(scan_param['tdac'])
            tdac_value = tdaclist[0]
        tdac_org = np.append(np.expand_dims(np.copy(self.dut.PixelConf['tdac0']) , axis=2), 
            np.expand_dims(np.copy(self.dut.PixelConf['tdac1']) , axis=2),
            axis=2)
        tdac = np.copy(tdac_org)
    
        rows = kwargs.pop("rows", local_configuration['rows'])
        if rows is None:
            rows = np.unique(np.argwhere(en_comp==1)[:,2])
        else:
            if isinstance(rows, int):
                rows = np.array([rows], dtype=np.int8)
            n_inj_row = min(kwargs.pop("n_inj_row", local_configuration['n_inj_row']), len(rows))
            n_loop_row = (len(rows) - 1) // n_inj_row + 1
            rowlist = np.ones([n_loop_row, n_inj_row], dtype='<u2') * 0xFFFF
            for i, r in enumerate(rows):
                div, mod = divmod(i, n_inj_row)
                rowlist[div, mod] = r
            param_dtype.append(("rows", "<u2", (n_inj_row)))
            scan_param['rows'] = rowlist
            nsteps = nsteps * n_loop_row
            rows = rowlist[0, :]

        cols = kwargs.pop("cols", local_configuration['cols'])
        if cols is None:
            cols = np.unique(np.argwhere(en_comp)[:,1])
        else:
            if isinstance(cols, int):
                cols = np.array([cols], dtype=np.int8)
            n_inj_col = min(kwargs.pop("n_inj_col", local_configuration['n_inj_col']), len(cols))
            n_loop_col = (len(cols) - 1) // n_inj_col + 1
            collist = np.ones([n_loop_col, n_inj_col], dtype='<u1') * 0xFF
            for i, c in enumerate(cols):
                div, mod = divmod(i, n_inj_col)
                collist[div, mod] = c
            param_dtype.append(("cols", "<u1", (n_inj_col)))
            scan_param['cols'] = collist
            nsteps = nsteps * n_loop_col
            cols = collist[0, :]

        for k in kwargs:
            param_dtype.append((k, '<u2'))
            scan_param[k] = kwargs[k]
            nsteps = nsteps * len(scan_param[k])

        ####################
        # create a table for scan_params
        param_dtype = np.dtype(param_dtype)
        self.scan_param_table = self.h5_file.create_table(
            self.h5_file.root,
            name='scan_parameters',
            title='scan_parameters',
            description=param_dtype,
            filters=self.filter_tables)
        for k, v in scan_param.items():
            self.meta_data_table.attrs[k] = v

        ts_n = self.dut['inj']['REPEAT']
        if ts_n == 0:
            self.logger.error('ERROR ERROR ERROR Set # of Inection RROR ERROR ERROR')
        self.dut.mask_rx(True)
        with self.readout(scan_param_id=-1,   # TODO open/close self.readout for each scan step (make it faster)
                          fill_buffer=True,
                          clear_buffer=True,
                          reset_rx=False,
                          reset_sram_fifo=False,  # set_rx, does these resets
                          readout_interval=0.001):
            dqdata = self.fifo_readout.data
            ####################
            # main scan loop
            self.logger.info('starting scan nsteps={}'.format(nsteps))
            for self.scan_param_id in range(nsteps):
                # update scan parameters
                scan_div = 1
                for pname, plist in scan_param.items():
                    plen = len(plist)
                    mod = (self.scan_param_id // scan_div) % plen
                    self.scan_param_table.row[pname] = plist[mod]
                    if self.scan_param_id // scan_div != (self.scan_param_id - 1) // scan_div:
                        self.logger.debug(self.scan_param_id, 'scan_div=', scan_div, 'plen=', plen, 'mod=', mod, end=' ')
                        if pname == 'inj':
                            if not is_plgen_external: # or plist[mod] < plsgen.SET['high']:
                                self.dut.mask_rx(True)
                            plsgen.set_inj_amp(plist[mod], unit='V')
                        elif pname == 'cols':
                            cols = plist[mod][plist[mod] != 0xFF]
                            self.dut.set_en_inj(cols=cols)
                        elif pname == 'rows':
                            rows = plist[mod][plist[mod] != 0xFFFF]
                        elif pname == 'tdac':
                            #self.logger.info('{} {} {}'.format(cols,comps, tdac_org))
                            tdac_value = plist[mod]
                        else:
                            self.dut.set_conf(**{pname: plist[mod]})
                    scan_div = scan_div * plen

                if 'tdac' in scan_param:
                    tdac[:, :] = tdac_org[:, :]
                    tdac[cols, rows, comps]=tdac_value

                if disable_noninjected_pixel:
                    en_comp[:, :, :] = 0
                    en_comp[cols, rows, comps] = 1
                self.dut.set_ram(en_comp, tdac)

                ####################
                # inject pulse and get data
                if dry_run:
                    continue
                self.dut.mask_rx(False)
                self.dut.inject()

                # count data
                ts_len = 0
                hit_len = 0
                while True:
                    time.sleep(0.01)
                    try:
                        data = dqdata.popleft()[0]
                        ts_len = ts_len + (data & 0xE000_0000 == 0x8000_0000).sum()
                        hit_len = hit_len + (data & 0xF000_0000 == 0x1000_0000).sum()
                        #self.logger.info('ts={} hit={} {}'.format(ts_len, hit_len,ts_n))
                    except IndexError:
                        if ts_len >= ts_n * 3:
                            break

                self.scan_param_table.row['scan_param_id'] = self.scan_param_id
                self.scan_param_table.row['timestamp'] = time.time()
                self.scan_param_table.row.append()
                self.scan_param_table.flush()

                self.dut.mask_rx(True)
                self.logger.info('InjectionScan: step={} ts={} hit={}'.format(self.scan_param_id, ts_len, hit_len))
                
            self.fifo_readout._data_buffer.clear()
            self.dut.set_ram(en_comp_org, tdac_org)

    def analyze(self):
        fraw = self.output_filename + '.h5'
        fhit = "_".join(fraw.split("_")[:-1])+"hit.h5"

        # interpret and event_build
        interpreter.interpret_h5(fraw, fhit)
        self.logger.info('interpreted {}'.format(fhit))
        event_builder.build_h5(fraw=fraw, fhit=fhit)
        self.logger.info('alingned to timestamp {}'.format(fhit))
        return fhit

    def plot(self):
        fraw = self.output_filename + '.h5'
        fhit = self.output_filename[:-4] + 'hit.h5'
        fpdf = self.output_filename + '.pdf'

if __name__ == "__main__":
    from panda2.panda2 import Panda2
    dut = Panda2()
    dut.init()
    dut.set_en_comp('all')
    #dut.set_sync(inj)
    dut.set_inj(inj_n=100, ext=True)#ivan from 100 to 20
    dut.set_inj_amp(0.1, unit='V')

    scan = InjectionScan(dut, online_monitor_addr="")
    scan.start(**local_configuration)
    scan.analyze()
    #scan.plot()
